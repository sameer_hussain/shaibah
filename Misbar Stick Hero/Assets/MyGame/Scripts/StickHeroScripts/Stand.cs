﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Stand : MonoBehaviour 
{
	private float screenWidth;

	private bool canMoveToStart;
	private bool canMoveToDestroy;
	private bool canMove;
	private  Vector3 target;//Move to this position
	public GameObject fruit;

	public bool canSpawnCherries;
	public bool canSpawnFruits;
	public float perfectLeft;
	public float perfectRight;

	void Start()
	{
		
		screenWidth = 2 * Camera.main.aspect * Camera.main.orthographicSize;
	//	Debug.Log ("screenwidth: " + -screenWidth);
		canMoveToStart = false;
		canMoveToDestroy = false;
		canSpawnCherries = false;
		canSpawnFruits = false;
		canMove = false;
	}

	public void Reset()
	{
		float posX = -screenWidth + GetComponent<BoxCollider2D>().size.x * transform.localScale.x/2;
		transform.position = new Vector3(posX, transform.position.y, transform.position.z);
	}

	public void MoveToStart()
	{
		float posX = -screenWidth/2f + GetComponent<BoxCollider2D>().size.x * transform.localScale.x/2;
		GameObject rotator = GameObject.Find("Rotator");
		if (rotator != null)
		{
			posX = rotator.transform.position.x - GetComponent<BoxCollider2D>().size.x * transform.localScale.x/2;
		}
		target = new Vector3 (posX, transform.position.y,  transform.position.z);
		canMoveToStart = true;
		//transform.DOMoveX(posX, 0.5f);
	}

	public void MoveToDestroy()
	{
		//transform.DOMoveX(-screenWidth, 0.5f).OnComplete(OnDestroy);
		target = new Vector3 (-screenWidth, transform.position.y,  transform.position.z);
		canMoveToDestroy = true;
	}

	public void MoveDistance(float distance)
	{
		//transform.DOMoveX(transform.position.x - screenWidth, 0.5f).OnComplete(CheckDestroy);
		target = new Vector3 (transform.position.x - screenWidth, transform.position.y,  transform.position.z);
		canMove = true;
	}

	void OnDestroy()
	{
		Destroy(gameObject);
	}

	void CheckDestroy()
	{
		if (transform.position.x < -screenWidth) { Destroy(gameObject); }
	}

	public void activateFruit()
	{
		fruit.GetComponent<MelonContoller> ().reActivateMelon ();
	}
		
	void Update ()
	{

		if (canMoveToStart) 
		{
			transform.position = Vector3.MoveTowards (transform.position, target, 10.0f * Time.deltaTime);
			//transform.position = Vector3.Lerp(transform.position, target, 0.5f);
			if (transform.position == target) 
			{
				
				canMoveToStart = false;
			}
		}
		else if (canMoveToDestroy)
		{
			transform.position = Vector3.MoveTowards (transform.position, target, 10.0f * Time.deltaTime);
			//transform.position = Vector3.Lerp(transform.position, target, 0.5f);
			if (transform.position == target) 
			{
				canMoveToDestroy = false;
				OnDestroy ();
			}
		}
		else if (canMove)
		{
			transform.position = Vector3.MoveTowards (transform.position, target, 10.0f * Time.deltaTime);
			//transform.position = Vector3.Lerp(transform.position, target, 0.5f);
			if (transform.position == target) 
			{
				canSpawnCherries = true;
				canSpawnFruits = true;
				canMove = false;

			}
		}
	}
}
