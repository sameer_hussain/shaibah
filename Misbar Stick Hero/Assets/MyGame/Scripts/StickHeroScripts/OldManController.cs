﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using ChartboostSDK;

public class OldManController : MonoBehaviour {

	public Rotator rotator;
	public CaneController cane;
	public StandSpawner standSpawner;
	public GameManager gm;


	private float offset = 0.2f; // Little distance between player and cane
	private Vector3 originalPos; // StartingPositon of Old man
	public  Vector3 target; //Move to this position
	public  Vector3 invertedTarget; //Move to this position when inverted

	private bool canMove; // To move after 
	private bool canMoveToOrigPos; 
	private bool canFall;
	public bool canInvert;

	public AudioClip footstep3;
	public AudioClip footstep5;
	public AudioClip perfectSound;
	public AudioClip invertSound;
	public AudioClip straightenSound;
	public AudioClip bottleCollectSound;
	public AudioClip fallSound;


	private Animator animator;     
	private float screenWidth;
	//private bool canShowAd;
	private int turns;
	//public GameUI gameUI;

	void Start () 
	{
		
		turns = PlayerPrefs.GetInt ("Turns");
		//canShowAd = false;

		if (PlayerPrefs.GetString ("canShowAds") == "Yes") 
		{
			Debug.Log("Interstial caching");
			Chartboost.didCacheInterstitial += Chartboost_didCacheInterstitial;
			PluginHelper_Ads._activStaticAd = StaticAdNetworkSelection.ChartBoost;
			PluginHelper_Ads._activeVideoAd = VideoAdNetworkSelection.ChartBoost;
			PluginHelper_Ads.cacheStaticAd("Default","ca-app-pub-8386987260001674/9875638345");
		}
		animator = GetComponent<Animator>();
		setCharacterAnimation ();
		screenWidth = 2 * Camera.main.aspect * Camera.main.orthographicSize;
		canMove = false;
		canInvert = false;
		transform.position = new Vector3(rotator.transform.position.x - offset, transform.position.y, transform.position.z);
		originalPos = transform.position;
		offset =  Mathf.Abs(cane.transform.position.x - transform.position.x);
		//Debug.Log("Offset = " + offset + " Position " + transform.position + "originalPos " + originalPos);
	}

	private void setCharacterAnimation() 
	{
		int characterSelected = PlayerPrefs.GetInt ("CharacterSelected");
		Debug.Log ("characterSelected " + characterSelected);
		if (characterSelected == 0) {
			
		} 
		else 
		{
			characterSelected = characterSelected + 1;
			string characterName = "Character" + characterSelected.ToString ();
			animator.SetBool (characterName,true);
		}

	}

	void Chartboost_didCacheInterstitial (CBLocation obj)
	{
		Debug.Log("didCacheInterstitial: " + obj);
	}

	public void movePlayer()
	{
		
		float stickLength = cane.GetComponent<BoxCollider2D>().size.y * cane.transform.localScale.y;
		float endX = originalPos.x + stickLength + offset;

		GameObject nextStand = standSpawner.nextStand;
		Vector2 colliderBox = nextStand.GetComponent<BoxCollider2D>().size * nextStand.transform.localScale.x;
		GameObject perfectMat = nextStand.transform.Find ("Perfect(Clone)").gameObject;
		Vector2 perfectMatColliderBox = perfectMat.GetComponent<BoxCollider2D> ().size * perfectMat.transform.localScale.x;
		float perfectLeft = perfectMat.transform.position.x - perfectMatColliderBox.x/2f;
		float perfectRight = perfectMat.transform.position.x + perfectMatColliderBox.x/2f;

		if (endX > perfectLeft && endX < perfectRight) 
		{
			SoundManager.instance.playSingle (perfectSound);
			gm.incrementScore ();
			gm.perfect ();
		} 

		if (endX > originalPos.x + screenWidth) 
		{
			target = new Vector3 (originalPos.x + screenWidth, originalPos.y, 0);
		} 
		else 
		{
			target = new Vector3 (endX, originalPos.y ,0);
		}
		canMove = true;
		SoundManager.instance.playFootsteps (footstep3, footstep5);
	}

	void fall()
	{
		
		float endY = transform.position.y - Camera.main.orthographicSize;
		target = new Vector3 (transform.position.x, transform.position.y - Camera.main.orthographicSize ,0);
		canFall = true;
		canInvert = false; // dont move when inverted


	}

	public void invertOldMan()
	{
		if (canMove) 
		{
			
			if (rotator.isRotating == false || canMoveToOrigPos == true)
			{
				GameObject nextStand = standSpawner.nextStand;
				Vector2 colliderBox = nextStand.GetComponent<BoxCollider2D>().size * nextStand.transform.localScale.x;
				float leftX = nextStand.transform.position.x - colliderBox.x/2f;
				float rightX = nextStand.transform.position.x + colliderBox.x/2f;

				GameObject currentStand = standSpawner.currentStand;
				Vector2 currentColliderBox = currentStand.GetComponent<BoxCollider2D>().size * nextStand.transform.localScale.x;
				float currentLeftX = currentStand.transform.position.x - colliderBox.x/2f;
				float currentRightX = currentStand.transform.position.x + colliderBox.x/2f;

				if (transform.position.x > currentLeftX && transform.position.x < currentRightX) 
				{
					canInvert = false;
				} 
				else if (transform.position.x > leftX) 
				{
					canInvert = false;
				} 
				else 
				{
					SoundManager.instance.playSingle (invertSound);
					canInvert = true;
					transform.localScale = new Vector3(1F, -1F, 1);
					float stickWidth = cane.GetComponent<BoxCollider2D> ().size.x;
					transform.position = new Vector3(transform.position.x, -3.3f , transform.position.z);


					if (leftX > target.x) 
					{
						invertedTarget = new Vector3(target.x, -3.3f , transform.position.z);
					} 
					else 
					{
						invertedTarget = new Vector3(leftX, -3.3f , transform.position.z);
					}
				}

			}
		}


	}

	public void straightenOldMan()
	{
		if (rotator.isRotating == false) 
		{
			SoundManager.instance.playSingle (straightenSound);

			transform.localScale = new Vector3 (1F, 1F, 1);
			float stickWidth = cane.GetComponent<BoxCollider2D> ().size.x;
			transform.position = new Vector3 (transform.position.x, originalPos.y, transform.position.z);
			target = new Vector3 (target.x, originalPos.y, transform.position.z);
			canInvert = false;
			canMove = true;
		}
	}

	void reset()
	{

		target = originalPos;
		canMoveToOrigPos = true;
	}

	void aliveCheck()
	{
		bool isAlive = false;
		GameObject nextStand = standSpawner.nextStand;
		Vector2 colliderBox = nextStand.GetComponent<BoxCollider2D>().size * nextStand.transform.localScale.x;
		float leftX = nextStand.transform.position.x - colliderBox.x/2f;
		float rightX = nextStand.transform.position.x + colliderBox.x/2f;

		float stickLength = cane.GetComponent<BoxCollider2D>().size.y * cane.transform.localScale.y;
		float endX = originalPos.x + stickLength + offset;


		//Debug.Log ("Transform: " + transform.position.x + " " + endX);

		if (transform.position.x > leftX && transform.position.x < rightX)
		{
			gm.incrementScore ();
			reset ();
			cane.reset ();
			rotator.Reset ();
			standSpawner.MoveStands ();
		}
		else 
		{
			gm.gameOver();
			rotator.Fall();
			fall();

		}
	}


	private void OnTriggerEnter2D (Collider2D other)
	{
		//Check if the tag of the trigger collided with is Cherry.
		if (other.tag == "Cherry") 
		{
			SoundManager.instance.playSingle (bottleCollectSound);
			Debug.Log ("Collided");
			other.gameObject.SetActive (false);
			gm.incrementCherries ();
		}
	}

	void showAd()
	{
		if (turns == 2) 
		{
			Debug.Log ("showing Ad called");
			if (PlayerPrefs.GetString ("canShowAds") == "Yes") 
			{
				Debug.Log ("show ad");
				PluginHelper_Ads.showStaticAd("Default");
				PlayerPrefs.SetInt ("Turns", 0);
			}

		}
	}

	void Update () 
	{
		if (canMove) 
		{
			animator.SetTrigger ("oldManWalk");
			transform.position = Vector3.MoveTowards (transform.position, target, 2.5f * Time.deltaTime);
			if (transform.position == target) 
			{
				SoundManager.instance.footstepsSource.Stop();
				canMove = false;
				aliveCheck ();
			} 
			else if (canInvert == true) 
			{
				canMove = false;
			}
		} 
		else if (canMoveToOrigPos) 
		{
			canInvert = false;
			gm.isOldManMoving = true;
			transform.position = Vector3.MoveTowards (transform.position, target, 10.0f * Time.deltaTime);
		//	transform.position = Vector3.Lerp(transform.position, target, 0.5f);
			if (transform.position == target) 
			{
				gm.isOldManMoving = false;
				gm.isGrowable = true;
				canMoveToOrigPos = false;
			}
		}
		else if (canFall)
		{	
			//canMove = false;
			transform.position = Vector3.MoveTowards (transform.position, target, 12.0f * Time.deltaTime);
			//	transform.position = Vector3.Lerp(transform.position, target, 0.5f);
			if (transform.position == target) 
			{
				SoundManager.instance.footstepsSource.Stop();
				SoundManager.instance.playSingle (fallSound);
				canFall = false;
				gm.canShake = true;
				showAd ();
			}
		}
		else if (canInvert == true) 
		{
			animator.SetTrigger ("oldManWalk");
			transform.position = Vector3.MoveTowards (transform.position, invertedTarget, 2.5f * Time.deltaTime);
			if (transform.position == invertedTarget) 
			{
				gm.gameOver();
				rotator.Fall();
				fall ();
			} 
		}

	}
}
