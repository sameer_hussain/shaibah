﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Rotator : MonoBehaviour 
{
	public bool isRotating;
	public OldManController oldMan;
	public GameObject oldManSmallCane;
	public GameObject stand;


	void Start()
	{
		Reset();
	}

	public void Reset()
	{
		float posX = -Camera.main.orthographicSize * Camera.main.aspect + stand.GetComponent<BoxCollider2D>().size.x/2f;
		transform.position = new Vector3(posX, transform.position.y, transform.position.z);

	}

	public void rotate()
	{
		isRotating = true;
		transform.DORotate(new Vector3(0, 0, -90), 0.3f).OnComplete(OnRotateComplete);
	}

	public void rotateForSplash()
	{
		isRotating = true;
		transform.DORotate (new Vector3 (0, 0, -160), 1f).OnStepComplete(check);
	}

	public void check ()
	{
		Debug.Log("Deeeeh");
	}

	public void Fall()
	{
		isRotating = true;
		transform.DORotate(new Vector3(0, 0, -180), 0.3f).OnComplete(OnFallComplete);

	}


	void OnRotateComplete()
	{
		//Debug.Log("Complete");
		//SoundManager.GetInstance().MakeLandingSound();
		oldMan.movePlayer();
		isRotating = false;
		oldManSmallCane.gameObject.SetActive (true);
	}

	void OnFallComplete()
	{
		isRotating = false;


	}
}
