﻿       using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StandSpawner : MonoBehaviour 
{
	public GameObject stand;
	public Transform neareast;
	public Transform farthest;
	public GameObject currentStand;
	public GameObject nextStand;
	public GameObject pendingStand;
	public GameObject cherriesPrefab;
	public GameObject perfectMatPrefab;
	
	private float screenWidth;

	void Start()
	{
	//	Debug.Log ("start Spawning");
		screenWidth = 2 * Camera.main.aspect * Camera.main.orthographicSize;
		float posX = -screenWidth / 2;
		Vector3 spawnPos = new Vector3(posX, stand.transform.position.y, stand.transform.position.z);
		currentStand = GameObject.Instantiate(stand, spawnPos, Quaternion.identity) as GameObject;
		currentStand.transform.parent = transform;
		nextStand = spawnStand(0f);
		pendingStand = spawnStand(screenWidth);

	}

	public GameObject spawnStand(float distance)
	{
		float randomX = Random.Range(neareast.position.x + distance, farthest.position.x + distance);
		Vector3 spawnPos = new Vector3(randomX, neareast.position.y, neareast.position.z);
		GameObject spawnStand = GameObject.Instantiate(stand, spawnPos, Quaternion.identity) as GameObject;
		spawnStand.transform.localScale += Random.Range(-0.8f, 0.2f) * Vector3.right;
		spawnStand.transform.parent = transform;

		GameObject perfectMat = spawnPerfectMat (spawnStand);
		perfectMat.transform.parent = spawnStand.transform;
		Vector2 perfectMatColliderBox = perfectMat.GetComponent<BoxCollider2D>().size * perfectMat.transform.localScale.x;
		float leftX = perfectMat.transform.position.x - perfectMatColliderBox.x / 2f;
		float rightX = perfectMat.transform.position.x + perfectMatColliderBox.x / 2f;
		spawnStand.GetComponent<Stand> ().perfectLeft = leftX;
		spawnStand.GetComponent<Stand> ().perfectRight = rightX;

		return spawnStand;
	}

	public void spawnCherries()
	{
	float prob = Random.Range(1, 100);
		if (prob > 75)
		{
			Vector2 nextColliderBox = nextStand.GetComponent<BoxCollider2D>().size * nextStand.transform.localScale.x;
			float leftX = nextStand.transform.position.x - nextColliderBox.x/2f - 0.5f;
			Vector2 currentColliderBox = currentStand.GetComponent<BoxCollider2D>().size * currentStand.transform.localScale.x;
			float rightX = currentStand.transform.position.x + currentColliderBox.x/2f + 0.5f;
			float randomX = Random.Range(rightX, leftX);
			Vector3 spawnPos = new Vector3(randomX, -3.2f, 0);
			GameObject cherry = GameObject.Instantiate(cherriesPrefab, spawnPos, Quaternion.identity) as GameObject;
		//	Debug.Log ("Spawn Cherries: " + spawnPos);
			cherry.transform.parent = nextStand.transform;
		}
	}

	public GameObject spawnPerfectMat (GameObject parentStand)
	{
		Vector2 nextColliderBox = parentStand.GetComponent<BoxCollider2D>().size * parentStand.transform.localScale.y;
	
		float topY = parentStand.transform.position.y + nextColliderBox.y / 2f;
	
		float x = parentStand.transform.position.x;
		float z = parentStand.transform.position.z;
		Vector3 spawnPos = new Vector3(x, topY, z);

		GameObject perfectMat = GameObject.Instantiate(perfectMatPrefab, spawnPos, Quaternion.identity) as GameObject;

	
		Vector2 selfColliderBox = perfectMat.GetComponent<BoxCollider2D> ().size * perfectMat.transform.localScale.y;
		float height = perfectMat.transform.position.y - selfColliderBox.y / 2f;
		perfectMat.transform.position = new Vector3 (perfectMat.transform.position.x, height, 
			transform.position.z);


		return perfectMat;
	}
	
	public void MoveStands()
	{
		currentStand.GetComponent<Stand>().MoveToDestroy();
		nextStand.GetComponent<Stand>().MoveToStart();
		pendingStand.GetComponent<Stand>().MoveDistance(screenWidth);
		currentStand = nextStand;
		nextStand = pendingStand;
		pendingStand = spawnStand(screenWidth);
	}

	void Update ()
	{
		if (nextStand.GetComponent<Stand> ().canSpawnCherries)
		{
			spawnCherries ();
			nextStand.GetComponent<Stand> ().canSpawnCherries = false;
		}
	}
}
