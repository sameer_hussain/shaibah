﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
	public bool isPause = false;
	public bool isGameOver = false;
	public bool isGrowable = true;
	public bool isOldManMoving = false;

	public int score;
	public int bestScore;
	public int cherriesScore;

	public GameUI gameUI;

	public Transform camTransform;
	public float shakeDuration; // How long the object should shake for.
	public float shakeAmount; // Amplitude of the shake. A larger value shakes the camera harder.
	public float decreaseFactor;
	public bool canShake = false;
	Vector3 originalPos;


	void Start()
	{
		isOldManMoving = false;
		bestScore = PlayerPrefs.GetInt("BestScore");
		cherriesScore = PlayerPrefs.GetInt ("CherriesScore");
		originalPos = camTransform.localPosition;
	}

	public void restart()
	{
		int turns = PlayerPrefs.GetInt ("Turns");
//		Debug.Log (turns);
		turns++;
		PlayerPrefs.SetInt ("Turns", turns);
		Application.LoadLevel("Game 1");
	}
	
	public void quit()
	{
		isPause = false;
		isGameOver = false;
		score = 0;
		Time.timeScale = 1f;
		Application.LoadLevel("Title");
	}

	public void gameOver()
	{
		saveData();
	}

	void saveData()
	{
		if (score > bestScore)
		{
			bestScore = score;
			PlayerPrefs.SetInt("BestScore", bestScore);
			PluginHelper_Core.submitScore ("Score", bestScore, null);
		}
	}

	public void incrementCherries()
	{
		Debug.Log ("Collides");
		cherriesScore += 1;
	//	Debug.Log (cherriesScore);
		PlayerPrefs.SetInt("CherriesScore", cherriesScore);
	}

	public void incrementScore()
	{
		score += 1;
	}

	public void perfect()
	{ 
		gameUI.showPerfect ();
	}

	void Update()
	{
		if (canShake == true)
		{
			if (shakeDuration > 0)
			{
				camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;

				shakeDuration -= Time.deltaTime * decreaseFactor;
			}
			else
			{
				shakeDuration = 1.0f;
				camTransform.localPosition = originalPos;
				canShake = false;
				isGameOver = true;
				gameUI.gameOver();
			}
		}
	}
}
