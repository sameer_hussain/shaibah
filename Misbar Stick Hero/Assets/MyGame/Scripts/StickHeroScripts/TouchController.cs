﻿using UnityEngine;
using System.Collections;

public class TouchController : MonoBehaviour 
{
	public CaneController cane;
	public OldManController oldMan;
	public GameManager gm;

	private bool isClicked = false;

	// Update is called once per frame
	void Update () 
	{
		if (!gm.isGameOver) 
		{
			if (gm.isGrowable) 
			{
				if (Input.GetMouseButtonDown (0) && !isClicked) 
				{
					isClicked = true;
					cane.ToScale ();
				}
				if (Input.GetMouseButtonUp (0) && isClicked) 
				{
					isClicked = false;
					cane.StopScale ();
				}
			}
			else if(oldMan.canInvert)
			{
				if (Input.GetMouseButtonDown (0)) 
				{
					oldMan.straightenOldMan ();
				}
			}
			else 
			{
				if (Input.GetMouseButtonDown (0)) 
				{
				//	Debug.Log ("invert it");
					oldMan.invertOldMan ();
				}
			}
		}
	}
}
