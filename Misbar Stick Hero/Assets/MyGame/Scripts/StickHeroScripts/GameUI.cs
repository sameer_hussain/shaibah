﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ArabicSupport;

public class GameUI : MonoBehaviour 
{
	public GameObject gameOverPanel;
	public Button playButton;
	public Button homeButton;

	public Text scoreText;
	public Text resultScore;
	public Text resultBestScore;
	public Button restartButton;
	public Text cherriesScore;
	public Text perfectText;
	public GameManager gm;

	public Text gameOverText;
	public Text scoreHeadingText;
	public Text bestScoreHeadingText;
	public Text howToPlayText;

	public Button shareButton; 

	private bool canShowPerfect;
	private float showAmount;
	private int decreaseFactor;
	void Start()
	{
		gameOverPanel.SetActive(false);
		canShowPerfect = false;
		perfectText.gameObject.SetActive (false);
		showAmount = 1.0f;
		decreaseFactor = 1;
		scoreText.gameObject.SetActive(true);
		scoreText.text = "" + gm.score;

		gameOverText.text = ArabicFixer.Fix ("لقد خسرت");
		scoreHeadingText.text = ArabicFixer.Fix ("النتيجة الحالية");
		bestScoreHeadingText.text = ArabicFixer.Fix ("أفضل نتيجة");
		perfectText.text = ArabicFixer.Fix ("أحسنت");
		howToPlayText.text = ArabicFixer.Fix ("استمر بالضغط على الشاشة لرفع العصا");
		shareButton.transform.Find ("Text").GetComponent<Text> ().text = ArabicFixer.Fix ("مشاركة");
	}

	public void setScoreText(int score)
	{
		if (score >= 1) 
		{
			howToPlayText.gameObject.SetActive (false);
		}
		scoreText.text = "" + score;
	}

	public void restart()
	{
		gm.restart();
		//Debug.Log("call fade");
		//GetComponent<FadeManager>().fade(true, 1.25f);
	}

	public void homeButtonPressed()
	{
		gm.quit();
	}

	void quit()
	{
		gm.quit();
	}

	public void setCherriesText(int cs)
	{
		cherriesScore.text = cs.ToString ();
	}

	public void setBestScoreText(int bs)
	{
		resultBestScore.text = bs.ToString ();
	}

	public void gameOver()
	{
		howToPlayText.gameObject.SetActive (false);
		gameOverPanel.SetActive(true);
		scoreText.gameObject.SetActive(false);
		resultScore.text = "" + gm.score;
		resultBestScore.text = "" + gm.bestScore;

	}

	public void showPerfect()
	{
		perfectText.gameObject.SetActive (true);
		canShowPerfect = true;
	}

	public void openShareDialog()
	{
		string textToShare = "Omg! I got " + gm.score + " points in الشيبة";
		GameObject.Find ("PluginCaller").GetComponent<NativeShare> ().ShareScreenshotWithText(textToShare);
	}

	public void showLeaderboard()
	{

		PluginHelper_Core.showLeaderboards ();
	}

	void Update()
	{
		setCherriesText(gm.cherriesScore);
		setScoreText(gm.score);
		setBestScoreText (gm.bestScore);
		if (canShowPerfect == true)
		{
			if (showAmount > 0)
			{
				showAmount -= Time.deltaTime * decreaseFactor;
			}
			else
			{
				perfectText.gameObject.SetActive (false);
				showAmount = 1.0f;
				canShowPerfect = false;
			}
		}
	}


}
