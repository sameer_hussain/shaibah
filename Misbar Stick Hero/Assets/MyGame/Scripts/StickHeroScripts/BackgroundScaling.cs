﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BackgroundScaling : MonoBehaviour 
{
	public GameObject background;
	public Sprite backgroundImage;
	float screenWidth;
	void Start ()
	{
		//int randomIndex = Random.Range(0, backgrounds.Count);
		GetComponent<SpriteRenderer>().sprite = backgroundImage;

		float bgWidth = background.GetComponent<BoxCollider2D>().size.x;
		float bgHeight = background.GetComponent<BoxCollider2D>().size.y;
		float screenHeight = Camera.main.orthographicSize * 2;
		screenWidth = screenHeight * Camera.main.aspect;
		float scaleRatioX = screenWidth/bgWidth;
		float scaleRatioY = screenHeight/bgHeight;
		
		transform.localScale = new Vector3(scaleRatioX, scaleRatioY, 1);
	}

	void Update ()
	{
		Vector3 target = new Vector3 (-screenWidth/2, transform.position.y, transform.position.z);
		transform.position = Vector3.MoveTowards (transform.position, target, 0.4f * Time.deltaTime);

	}
}
