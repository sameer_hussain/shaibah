﻿using UnityEngine;
using System.Collections;

public class CaneController : MonoBehaviour {

	public Rotator rotator;
	public GameObject oldManSmallCane;
	public bool isScaling = false;
	private Vector3 originalPos;
	private float length;
	public GameManager gm;

	public AudioClip toScale;
	public AudioClip toStop;

	void Start()
	{
		isScaling = false;
		originalPos = transform.position;
		length = GetComponent<BoxCollider2D>().size.y;
		reset();
	}

	public void reset()
	{
		transform.localScale = new Vector3(1, 0, 1);
	}


	public void ToScale()
	{
		SoundManager.instance.playFootsteps (toScale);
		isScaling = true;
		rotator.transform.eulerAngles = Vector3.zero;
		reset();
	}

	public void StopScale()
	{
		SoundManager.instance.playSingle (toStop);

		isScaling = false;
		gm.isGrowable = false;
		rotator.rotate();
	}

	void Scale()
	{
		transform.localScale += Vector3.up * 0.6f * Time.deltaTime;
		transform.localPosition = Vector3.up * length/2f * transform.localScale.y;
		oldManSmallCane.gameObject.SetActive (false);
	}



	void Update()
	{
		/*if (isScaling) 
		{ 
			transform.localScale += Vector3.up * 0.3f * Time.deltaTime;
			transform.localPosition = Vector3.up * length/2f * transform.localScale.y;
		}*/
		if (isScaling) 
		{ 
			Scale(); 
		}
	}
}
