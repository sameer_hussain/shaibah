﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;
using ArabicSupport;


public class BottleShop : MonoBehaviour 
{
	public Text bottleCount; 
	public Text shopText;
	public Button disableBottlesButton;
	public Button restoreInAppButton;
	private float screenHeight;
	private float screenWidth;
	private bool moveToTop;
	private bool moveToBottom;
	// Use this for initialization
	private static Action<string> _purchaseRestorationAction;
	void Start () 
	{
		_purchaseRestorationAction += restoreCallback;
		screenHeight = Camera.main.orthographicSize * 2;
		screenWidth = screenHeight * Camera.main.aspect;
		transform.position = new Vector3 (transform.position.x, -screenHeight, transform.position.z);
		shopText.text = ArabicFixer.Fix("السوق");
		disableBottlesButton.transform.Find ("Text").GetComponent<Text> ().text = ArabicFixer.Fix ("تعطيل القوارير");
		restoreInAppButton.transform.Find ("Text").GetComponent<Text> ().text = ArabicFixer.Fix ("إعادة المشتريات");
		//

		//gameObject.SetActive (true);
	}

	public void restoreCallback(string data)
	{
		Debug.Log ("Restore Data::::"+ data);
		if (data == "no_Ads")
		{
			PlayerPrefs.SetString("canShowAds", "No");
			Debug.Log ("No Ads");
		}
	}

	public void showBottleShop()
	{
		moveToBottom = false;
		moveToTop = true;
	}

	public void hideBottleShop()
	{
		moveToBottom = true;
	}
	// Update is called once per frame
	void Update () 
	{
		bottleCount.text = PlayerPrefs.GetInt ("CherriesScore").ToString();
		if (moveToTop == true) 
		{
			//Debug.Log ("Move to top");
			Vector3 targetA = new Vector3 (transform.position.x, 0, transform.position.z);
			transform.position = Vector3.MoveTowards (transform.position, targetA, 30f * Time.deltaTime);
			if (transform.position == targetA) 
			{
				moveToTop = false;
			}   
		}

		if (moveToBottom) 
		{
			//Debug.Log ("Move to bottom");
			Vector3 targetB = new Vector3 (transform.position.x, -screenHeight, transform.position.z);
			transform.position = Vector3.MoveTowards (transform.position, targetB, 30f * Time.deltaTime);
			if (transform.position == targetB) 
			{
				moveToBottom = false;
				gameObject.SetActive (false);
			}
		}
	}

	public void add400Bottles()
	{
		PluginHelper_Core.purchaseConsumableProduct ("bottle400");
	}

	public void add1000Bottles()
	{
		PluginHelper_Core.purchaseConsumableProduct ("bottle1000");
	}
		
	public void add3000Bottles()
	{
		PluginHelper_Core.purchaseConsumableProduct ("bottles3000");
	}

	public void add5000Bottles()
	{
		PluginHelper_Core.purchaseConsumableProduct ("bottles5000");
	}



	public void restoreInApp()
	{
		PluginHelper_Core.restoreCompletedTransactions (_purchaseRestorationAction);
	}
}
