﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HeroSelectionButton : MonoBehaviour {

	public int characterNumber;
	public Text price;
	public Image bottleImage;
	public Image heroImage;
	public GameObject bottlesShop;
	public bool isUnlocked; 

	// Use this for initialization
	void Start () 
	{
		//GameObject.onClick.AddListener(playGame);
		//isUnlocked = false;
	}

	public void unlock()
	{
		isUnlocked = true;
		price.gameObject.SetActive (false);
		bottleImage.gameObject.SetActive (false);
		heroImage.gameObject.SetActive (true);
	}

	public void heroSelected()
	{
		Debug.Log (isUnlocked);
		if (isUnlocked == true) 
		{
			PlayerPrefs.SetInt("CharacterSelected", characterNumber);
			GameObject.Find ("CharacterShop").GetComponent<CharacterShop>().hideCharacterShop();

			//gameObject.SetActive (false);
		} 
		else 
		{
			int cherriesAvailable = PlayerPrefs.GetInt ("CherriesScore");
			int cherriesNeeded = int.Parse(price.text);
			if (cherriesAvailable >= cherriesNeeded) 
			{
				PlayerPrefs.SetInt("CharacterSelected", characterNumber);
				string characters = PlayerPrefs.GetString ("CharactersUnlocked");
				characters = characters + characterNumber + "%";
				PlayerPrefs.SetString ("CharactersUnlocked", characters);
				cherriesAvailable = cherriesAvailable - cherriesNeeded;
				PlayerPrefs.SetInt ("CherriesScore", cherriesAvailable);
				int x = characterNumber + 1;
				string characterSpriteName = "Character" + x.ToString() + "_Idle_1";
				unlock ();
			} 
			else 
			{
				GameObject.Find ("CharacterShop").GetComponent<CharacterShop>().hideCharacterShop();
				bottlesShop.SetActive (true);
				bottlesShop.GetComponent<BottleShop> ().showBottleShop ();	

			}
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
