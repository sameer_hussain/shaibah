﻿using UnityEngine;
using System.Collections;



public class StandSpawnerForSplash : MonoBehaviour 
{
	public GameObject stand;
	public Transform neareast;
	public Transform farthest;
	public GameObject currentStand;
	public GameObject nextStand;
	public GameObject pendingStand;
	public GameObject fruitsPrefab;
	public GameObject cherryPrefab;
	//public Rotator rotator;

	private float screenWidth;
	// Use this for initialization
	void Start () 
	{
		screenWidth = 2 * Camera.main.aspect * Camera.main.orthographicSize;
		float posX = -screenWidth / 2;
		//Vector3 spawnPos = new Vector3(rotator.transform.position.x + 1f, stand.transform.position.y, stand.transform.position.z);
		Vector3 spawnPos = new Vector3(posX, stand.transform.position.y, stand.transform.position.z);
		currentStand = GameObject.Instantiate(stand, spawnPos, Quaternion.identity) as GameObject;
		currentStand.transform.parent = transform;
		//currentStand.transform.localScale += -0.8f * Vector3.right;
		nextStand = spawnStand(0f);
		spawnFruits ();
		pendingStand = spawnStand(screenWidth);
	}

	public GameObject spawnStand(float distance)
	{
		float randomX = Random.Range(neareast.position.x + distance, farthest.position.x + distance);
		Vector3 spawnPos = new Vector3(randomX, neareast.position.y, neareast.position.z);
		GameObject spawnStand = GameObject.Instantiate(stand, spawnPos, Quaternion.identity) as GameObject;
		spawnStand.transform.localScale += -0.8f * Vector3.right;
		spawnStand.transform.localScale += Random.Range(0, 2.5f) * Vector3.up;
		//spawnStand.transform.localScale += 0 * Vector3.up;
		//spawnStand.transform.position = new Vector3(transform.position.x, Random.Range(-4.5f, -6.5f), transform.position.z);
		spawnStand.transform.parent = transform;
		GameObject cherry = spawnCherries (spawnStand);
		cherry.transform.parent = spawnStand.transform;
		return spawnStand;
	}

	public void spawnFruits()
	{
		Vector2 nextColliderBoxWidth = nextStand.GetComponent<BoxCollider2D>().size * nextStand.transform.localScale.x;
		float leftX = nextStand.transform.position.x - nextColliderBoxWidth.x/2f;
		Vector2 nextColliderBoxHeight = nextStand.GetComponent<BoxCollider2D>().size * nextStand.transform.localScale.y;
		float leftY = nextStand.transform.position.y + nextColliderBoxHeight.y/2f;

		Vector2 currentColliderBoxWidth = currentStand.GetComponent<BoxCollider2D>().size * currentStand.transform.localScale.x;
		float rightX = currentStand.transform.position.x + currentColliderBoxWidth.x/2f;
		Vector2 currentColliderBoxHeight = currentStand.GetComponent<BoxCollider2D>().size * currentStand.transform.localScale.y;
		float rightY = currentStand.transform.position.y + currentColliderBoxHeight.y/2f;

	//	Debug.Log (rightX + "," + rightY + " @ " + leftX + "," + leftY);

		float radius = Vector3.Distance(new Vector3(leftX, rightY, 0), new Vector3(rightX, rightY, 0));
		//Debug.Log (radius);

		Vector3 pos = randomCircle(new Vector3(rightX, rightY, 0),radius);
		GameObject fruit = GameObject.Instantiate(fruitsPrefab, pos, Quaternion.identity) as GameObject;
		fruit.GetComponent<MelonContoller> ().origPos = pos;
		float random = Random.Range (-0.2f, 0f);
		Debug.Log (random);
		fruit.transform.localScale += random * Vector3.right;
		fruit.transform.localScale += random * Vector3.up;
		/*Debug.Log (Vector3.Distance(cherriesPrefab.transform.position, new Vector3(rightX, rightY, 0)));*/
		fruit.transform.parent = currentStand.transform;
		currentStand.GetComponent<Stand> ().fruit = fruit;
	}

	public GameObject spawnCherries (GameObject parentStand)
	{
		float prob = Random.Range(1, 100);
		Vector2 nextColliderBox = parentStand.GetComponent<BoxCollider2D> ().size * parentStand.transform.localScale.y;

		float topY = parentStand.transform.position.y + nextColliderBox.y / 2f;

		float x = parentStand.transform.position.x;
		float z = parentStand.transform.position.z;
		Vector3 spawnPos = new Vector3 (x, topY + 0.12f, z);

		GameObject cherry = GameObject.Instantiate (cherryPrefab, spawnPos, Quaternion.identity) as GameObject;
		cherry.SetActive (false);
		if (prob > 75) 
		{
			cherry.SetActive (true);
		}
		return cherry;
	}

	Vector3 randomCircle ( Vector3 center ,   float radius  )
	{
		float ang = Random.Range (20, 140);
		Vector3 pos;
		pos.x = center.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
		pos.y = center.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad);
		Debug.Log (pos.y);
		if (pos.y < -5) 
		{
			pos.y = pos.y + 1;
		}
		if (pos.y > 4) 
		{
			pos.y = pos.y - 0.5f;
		}
		pos.z = center.z;
		return pos;
	}
	
	public void moveStands()
	{
		currentStand.GetComponent<Stand>().MoveToDestroy();
		nextStand.GetComponent<Stand>().MoveToStart();
		pendingStand.GetComponent<Stand>().MoveDistance(screenWidth);
		currentStand = nextStand;
		nextStand = pendingStand;
		pendingStand = spawnStand(screenWidth);

	}

	void Update ()
	{
		if (nextStand.GetComponent<Stand> ().canSpawnFruits)
		{
			spawnFruits ();
			nextStand.GetComponent<Stand> ().canSpawnFruits = false;
		}
	}


}
