﻿using UnityEngine;
using System.Collections;

public class GameManagerForSplash : MonoBehaviour {

	public bool isGameOver = false;
	public bool isGrowable = true;
	public bool isOldManMoving = false;

	public int score;
	public int bestScoreForSplash;
	public int cherriesScore;

	public Transform camTransform;
	public float shakeDuration; // How long the object should shake for.
	public float shakeAmount; // Amplitude of the shake. A larger value shakes the camera harder.
	public float decreaseFactor;
	public bool canShake = false;
	Vector3 originalPos;

	public GameUIForSplash gameUI;
	// Use this for initialization
	void Start () 
	{
		//PlayerPrefs.DeleteAll ();
		bestScoreForSplash = PlayerPrefs.GetInt("BestScoreForSplash", 0);
		cherriesScore = PlayerPrefs.GetInt ("CherriesScore");
		originalPos = camTransform.localPosition;
	}

	public void restart()
	{
		isGameOver = false;
		isGrowable = true;
		score = 0;
		Application.LoadLevel("Game 2");
	}

	public void incrementCherries()
	{
		cherriesScore += 1;
		PlayerPrefs.SetInt("CherriesScore", cherriesScore);
	}

	public void incrementScore()
	{
		score += 1;
	}

	public void gameOver()
	{
		saveData();
		isGameOver = true;
		gameUI.gameOver();

	}

	public void Quit()
	{
		isGameOver = false;
		score = 0;
		Time.timeScale = 1f;
		Application.LoadLevel("Title");
	}


	void saveData()
	{
		if (score > bestScoreForSplash)
		{
			bestScoreForSplash = score;
			PlayerPrefs.SetInt("BestScoreForSplash", bestScoreForSplash);
			PluginHelper_Core.submitScore ("Melons", bestScoreForSplash, null);
		}
	}

	void Update()
	{
		if (canShake == true)
		{
			if (shakeDuration > 0)
			{
				camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;

				shakeDuration -= Time.deltaTime * decreaseFactor;
			}
			else
			{
				shakeDuration = 1.0f;
				camTransform.localPosition = originalPos;
				canShake = false;
				/*isGameOver = true;
				gameUI.GameOver();*/
			}
		}
	}
}
