﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class OldManForSplash : MonoBehaviour {
	public RotatorForSplash rotator;
	public CaneControllerForSplash cane;
	public StandSpawnerForSplash standSpawner;
	public GameManagerForSplash gm;
	public GameObject oldManSmallCane;

	private float offset = 0.2f; // Little distance between player and cane
	private Vector3 originalPos; // StartingPositon of Old man
	public  Vector3 target; //Move to this position
	public  Vector3 invertedTarget; //Move to this position when inverted
	public AudioClip jumpSound;
	private bool canMove; // To move after 
	private bool canMoveToOrigPos; 
	public bool canInvert;

	private Animator animator;     
	private float screenWidth;
	//public GameUI gameUI;

	void Start () 
	{
		screenWidth = 2 * Camera.main.aspect * Camera.main.orthographicSize;
		canMove = false;
		canInvert = false;
		transform.position = new Vector3(rotator.transform.position.x - offset, transform.position.y, transform.position.z);
		originalPos = transform.position;
		offset =  Mathf.Abs(cane.transform.position.x - transform.position.x);
		oldManSmallCane.gameObject.SetActive (false);
		animator = GetComponent<Animator>();
		setCharacterAnimation ();
		//standSpawner.spawnFirstStand ();
		//Debug.Log("Offset = " + offset + " Position " + transform.position + "originalPos " + originalPos);
	}

	private void setCharacterAnimation() 
	{
		int characterSelected = PlayerPrefs.GetInt ("CharacterSelected");
		Debug.Log ("characterSelected " + characterSelected);
		if (characterSelected == 0) {

		} 
		else 
		{
			characterSelected = characterSelected + 1;
			string characterName = "Character" + characterSelected.ToString ();
			animator.SetBool (characterName,true);
			//animator.SetBool ("Character5",true);
		}

	}
	public void jumpToSamePosition()
	{
		oldManSmallCane.gameObject.SetActive (true);
		transform.DOJump (transform.position, 2f, 1, 1f, false).OnComplete (jumpToSameComplete);
		//transform.DORotate (new Vector3(0, -50, 0), 1f);
	}

	public void jumptoNext()
	{
		Vector2 nextColliderBox = standSpawner.nextStand.GetComponent<BoxCollider2D>().size * 
			standSpawner.nextStand.transform.localScale.y;
		float x = standSpawner.nextStand.transform.position.x;
		float y = standSpawner.nextStand.transform.position.y + nextColliderBox.y / 2f;
		Vector2 selfColliderBox = GetComponent<BoxCollider2D> ().size * transform.localScale.y;
		y = y + selfColliderBox.y / 2f;
		transform.DOJump (new Vector3 (x, y, transform.position.z), 0.8f, 1, 1f, false).OnComplete(jumptoNextComplete);
		rotator.reset (y- 0.3f);
		oldManSmallCane.gameObject.SetActive (true);
	}


	public void jumptoNextComplete()
	{
		oldManSmallCane.gameObject.SetActive (false);
		SoundManager.instance.playSingle (jumpSound);
		target = new Vector3(originalPos.x, transform.position.y, transform.position.z);
		canMoveToOrigPos = true;
		standSpawner.moveStands ();
	}

	public void jumpToSameComplete()
	{
		oldManSmallCane.gameObject.SetActive (false);
		cane.firstTurnOver();
	}

	private void OnTriggerEnter2D (Collider2D other)
	{
		//Check if the tag of the trigger collided with is Cherry.
		if (other.tag == "Cherry") 
		{
			Debug.Log ("Collided");
			other.gameObject.SetActive (false);
			gm.incrementCherries ();
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (canMoveToOrigPos) 
		{
			gm.isOldManMoving = true;
			canInvert = false;
			transform.position = Vector3.MoveTowards (transform.position, target, 10.0f * Time.deltaTime);
			if (transform.position == target) 
			{
				gm.isOldManMoving = false;
				gm.isGrowable = true;
				cane.isScaling = true;
				canMoveToOrigPos = false;
			}
		}
	}
}
