﻿using UnityEngine;
using System.Collections;

public class TouchControllerForSplash : MonoBehaviour {
	public CaneControllerForSplash cane;
	public OldManForSplash oldMan;
	public GameManagerForSplash gm;

	// Use this for initialization
	void Update () 
	{
		if (!gm.isGameOver) 
		{
			if (Input.GetMouseButtonDown (0) && cane.isScaling) 
			{
				cane.stopScale ();
			}
		}
	}
}
