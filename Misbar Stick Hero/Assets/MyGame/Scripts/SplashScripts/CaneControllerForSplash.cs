﻿using UnityEngine;
using System.Collections;

public class CaneControllerForSplash : MonoBehaviour 
{
	public RotatorForSplash rotator;
	public StandSpawnerForSplash standSpawner;
	public bool isScaling;
	public bool canScaleUp;
	public OldManForSplash oldMan;
	private Vector3 originalPos;
	private float length;
	public GameManagerForSplash gm;
	public AudioClip burstWaterMelonSound;
	public AudioClip deathSound;


	private int turnCount;
	private int slicedFruit;
	private float screenWidth;
	private bool touchedPlatform;
	private bool touchedFruit;

	void Start()
	{
		isScaling = true;
		screenWidth = 2 * Camera.main.aspect * Camera.main.orthographicSize;
		canScaleUp = true;
		originalPos = transform.position;
		length = GetComponent<BoxCollider2D>().size.y;

		reset();
	}

	public void reset()
	{
		transform.localScale = new Vector3(1, 0, 1);
		touchedPlatform = false;
		touchedFruit = false;
		slicedFruit = 0;
	}

	void scaleUp()
	{
		
		float stickLength = length * transform.localScale.y;
		//Debug.Log (stickLength);
		if (stickLength < screenWidth - screenWidth/4) 
	  	{
			transform.localScale += Vector3.up * 0.4f * Time.deltaTime;
			transform.localPosition = Vector3.up * length / 2f * transform.localScale.y;
		} 
		else 
		{
			canScaleUp = false;
		}

	}

	void scaleDown()
	{

		float stickLength = length * transform.localScale.y;
		if (stickLength > 0) 
		{
			transform.localScale += Vector3.up * -0.4f * Time.deltaTime;
			transform.localPosition = Vector3.up * length / 2f * transform.localScale.y;
		} 
		else 
		{
			canScaleUp = true;
		}
	}

	public void stopScale()
	{
		isScaling = false;
		gm.isGrowable = false;
		rotator.rotateForSplash();
	}

	private void OnTriggerEnter2D (Collider2D other)
	{
		if (other.tag == "Fruit") 
		{
			Debug.Log ("Collided");
			if (slicedFruit == 0) 
			{
				slicedFruit++;
				SoundManager.instance.playSingle (burstWaterMelonSound);
			}

			//other.gameObject.SetActive (false);
			touchedFruit = true;
			other.gameObject.GetComponent<MelonContoller> ().melonFall ();
		}
		if (other.tag == "Stand") 
		{
			Debug.Log ("Collided with stand");
			rotator.isRotating = false;

			if (other.gameObject == standSpawner.currentStand) 
			{
				Debug.Log ("Colliding with currentStand");
			}
			else
			{
				SoundManager.instance.playSingle (deathSound);
				touchedPlatform = true;
				checkAlive ();
			}
		}
	}

	public void firstTurnOver ()
	{
		//Debug.Log ("You have a chance");
		isScaling = true;
		reset ();
		rotator.reset (rotator.transform.position.y);
		standSpawner.currentStand.GetComponent<Stand> ().activateFruit ();
		turnCount++;
	}

	public void checkAlive()
	{
		if (turnCount == 0) 
		{
			if (touchedPlatform == false && touchedFruit == false) 
			{
				SoundManager.instance.playSingle (deathSound);
				gm.canShake = true;
				oldMan.jumpToSamePosition ();
			}
			else if (touchedPlatform == true && touchedFruit == false) 
			{
				gm.canShake = true;
				oldMan.jumpToSamePosition ();
			}
			else if (touchedPlatform == true && touchedFruit == true) 
			{
				gm.canShake = true;
				oldMan.jumpToSamePosition ();
			}
			else if (touchedPlatform == false && touchedFruit == true) 
			{
				turnCount = 0;
				reset ();
				oldMan.jumptoNext ();
				gm.incrementScore ();
			}
		} 
		else if (turnCount == 1) 
		{
			if (touchedPlatform == false && touchedFruit == false) 
			{
				SoundManager.instance.playSingle (deathSound);
				gm.canShake = true;
				gm.gameOver ();
				Debug.Log ("Gameover");
			}
			else if (touchedPlatform == true && touchedFruit == false) 
			{
				gm.canShake = true;
				gm.gameOver ();
				Debug.Log ("Gameover");
			}
			else if (touchedPlatform == true && touchedFruit == true) 
			{
				gm.canShake = true;
				gm.gameOver ();
				Debug.Log ("Gameover");
			}
			else if (touchedPlatform == false && touchedFruit == true) 
			{
				turnCount = 0;
				reset ();
				oldMan.jumptoNext ();
				gm.incrementScore ();
			}
		}

	}
	
	// Update is called once per frame
	void Update () 
	{	
		if (isScaling)
		{
			if (canScaleUp == true) 
			{
				scaleUp (); 
			} 
			else 
			{
				scaleDown ();
			}
		}
	}
}
