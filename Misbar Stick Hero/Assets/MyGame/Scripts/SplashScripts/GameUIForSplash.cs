﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ArabicSupport;

public class GameUIForSplash : MonoBehaviour 
{

	public GameObject gameOverPanel;
	//public Button playButton;
	public Button homeButton;
	public Text scoreText;
	public Text resultScore;
	public Text resultBestScore;
	public Button restartButton;
	public Text cherriesScore;

	public Text gameOverText;
	public Text scoreHeadingText;
	public Text bestScoreHeadingText;
	public Button shareButton; 
	public Text howToPlayText;

	public GameManagerForSplash gm;
	// Use this for initialization
	void Start()
	{
		gameOverPanel.SetActive(false);
		scoreText.gameObject.SetActive(true);
		scoreText.text = "" + gm.score;
		gameOverText.text = ArabicFixer.Fix ("لقد خسرت");
		scoreHeadingText.text = ArabicFixer.Fix ("النتيجة الحالية");
		bestScoreHeadingText.text = ArabicFixer.Fix ("أفضل نتيجة");
		howToPlayText.text = ArabicFixer.Fix ("المس الشاشة لقطع البطيخ بدون لمس العمود المقابل ");
		shareButton.transform.Find ("Text").GetComponent<Text> ().text = ArabicFixer.Fix ("مشاركة");


	}

	public void restart()
	{
		gm.restart();
	}

	public void homeButtonPressed()
	{
		gm.Quit();
	}

	public void SetScoreText(int score)
	{
		if (score >= 1) 
		{
			howToPlayText.gameObject.SetActive (false);
		}
		scoreText.text = "" + score;
	}

	public void setCherriesText(int cs)
	{
		cherriesScore.text = cs.ToString ();
	}

	public void setBestScoreText(int bs)
	{
		resultBestScore.text = bs.ToString ();
	}

	public void gameOver()
	{
		howToPlayText.gameObject.SetActive (false);
		gameOverPanel.SetActive(true);
		scoreText.gameObject.SetActive(false);
		resultScore.text = "" + gm.score;
		resultBestScore.text = "" + gm.bestScoreForSplash;
	}

	public void openShareDialog()
	{
		string textToShare = "Omg! I got " + gm.score + " points in الشيبة";
		GameObject.Find ("PluginCaller").GetComponent<NativeShare> ().ShareScreenshotWithText(textToShare);
	}

	public void showLeaderboard()
	{
		PluginHelper_Core.showLeaderboards ();
	}
	// Update is called once per frame
	void Update () 
	{
		setCherriesText(gm.cherriesScore);
		SetScoreText(gm.score);
	}
}
