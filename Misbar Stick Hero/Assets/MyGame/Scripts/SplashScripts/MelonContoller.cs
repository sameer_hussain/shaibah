﻿using UnityEngine;
using System.Collections;

public class MelonContoller : MonoBehaviour {

	public GameObject rearView;
	public GameObject splashGameobject;
	public Vector3 origPos;
	public bool canFall;
	// Use this for initialization
	void Start () 
	{
		hideRearView ();
	}

	public void hideRearView()
	{
		splashGameobject.gameObject.SetActive (false);
		rearView.gameObject.SetActive (false);
	}

	public void melonFall()
	{
		splashGameobject.gameObject.SetActive (true);
		rearView.gameObject.SetActive (true);
		canFall = true;
	}

	public void reActivateMelon()
	{
		
		canFall = false;
		hideRearView ();
		transform.position = origPos;
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 target = new Vector3 (transform.position.x, transform.position.y - Camera.main.orthographicSize ,0);
		if (canFall) 
		{
			transform.position = Vector3.MoveTowards (transform.position, target, 12.0f * Time.deltaTime);
			if (transform.position == target) 
			{
				canFall = false;
			}
		}
	}
}
