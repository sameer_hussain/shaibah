﻿using UnityEngine;
using System.Collections;

public class ScaleBackgroundForSplash : MonoBehaviour {

	public GameObject firstBackground;
	public GameObject secondBackground;

	public GameObject firstForeground;
	public GameObject secondForeground; 

	public GameObject cloud2;
	public GameObject cloud1;

	public GameManagerForSplash gm;

	private float screenHeight;
	private float screenWidth;
	// Use this for initialization
	void Start () 
	{
		SpriteRenderer sr = firstBackground.GetComponent<SpriteRenderer> ();

		float width = sr.sprite.bounds.size.x;
		float height = sr.sprite.bounds.size.y;
		screenHeight = Camera.main.orthographicSize * 2;
		screenWidth = screenHeight * Camera.main.aspect;

		float scaleRatioX = screenWidth / width;
		float scaleRatioY = screenHeight / height;

		firstBackground.transform.localScale = new Vector3 (scaleRatioX, scaleRatioY, 1);
		secondBackground.transform.localScale = new Vector3 (scaleRatioX, scaleRatioY, 1);
		secondBackground.transform.position = new Vector3 (screenWidth, secondBackground.transform.position.y, secondBackground.transform.position.z);
		firstForeground.transform.localScale = new Vector3 (scaleRatioX, scaleRatioY, 1);
		secondForeground.transform.localScale = new Vector3 (scaleRatioX, scaleRatioY, 1);
		secondForeground.transform.position = new Vector3 (screenWidth, secondForeground.transform.position.y, secondForeground.transform.position.z);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (gm.isOldManMoving)
		{
			Vector3 target = new Vector3 (-screenWidth, firstBackground.transform.position.y, firstBackground.transform.position.z);
			/*SpriteRenderer sr = firstBackground.GetComponent<SpriteRenderer> ();

			float width = sr.sprite.bounds.size.x;
			float height = sr.sprite.bounds.size.y;*/

			firstBackground.transform.position = Vector3.MoveTowards (firstBackground.transform.position, target, 0.3125f * Time.deltaTime);
			if (firstBackground.transform.position == target) 
			{
				firstBackground.transform.position = new Vector3 (secondBackground.transform.position.x + screenWidth - 0.025f , firstBackground.transform.position.y, firstBackground.transform.position.z);
			}

			secondBackground.transform.position = Vector3.MoveTowards (secondBackground.transform.position, target, 0.3125f * Time.deltaTime);
			if (secondBackground.transform.position == target) 
			{
				secondBackground.transform.position = new Vector3 (firstBackground.transform.position.x + screenWidth - 0.025f , secondBackground.transform.position.y, secondBackground.transform.position.z);
			}

			firstForeground.transform.position = Vector3.MoveTowards (firstForeground.transform.position, target, 1.25f * Time.deltaTime);
			if (firstForeground.transform.position == target) 
			{
				firstForeground.transform.position = new Vector3 (secondForeground.transform.position.x + screenWidth - 0.025f , firstForeground.transform.position.y, firstForeground.transform.position.z);
			}

			secondForeground.transform.position = Vector3.MoveTowards (secondForeground.transform.position, target, 1.25f * Time.deltaTime);
			if (secondForeground.transform.position == target) 
			{
				secondForeground.transform.position = new Vector3 (firstForeground.transform.position.x + screenWidth - 0.025f, secondForeground.transform.position.y, secondForeground.transform.position.z);
			}


		}
		Vector3 cloudTarget1 = new Vector3 (-screenWidth - 1f, cloud1.transform.position.y, cloud1.transform.position.z);
		cloud1.transform.position = Vector3.MoveTowards (cloud1.transform.position, cloudTarget1, 0.25f * Time.deltaTime);
		if (cloud1.transform.position == cloudTarget1) 
		{
			cloud1.transform.position = new Vector3 (screenWidth, cloud1.transform.position.y, cloud1.transform.position.z);
		}
		Vector3 cloudTarget2 = new Vector3 (-screenWidth - 1f, cloud2.transform.position.y, cloud2.transform.position.z);
		cloud2.transform.position = Vector3.MoveTowards (cloud2.transform.position, cloudTarget2, 0.25f * Time.deltaTime);
		if (cloud2.transform.position == cloudTarget2) 
		{
			cloud2.transform.position = new Vector3 (screenWidth, cloud2.transform.position.y, cloud2.transform.position.z);
		}
	}
}
