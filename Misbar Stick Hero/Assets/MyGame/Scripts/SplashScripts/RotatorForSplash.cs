﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class RotatorForSplash : MonoBehaviour {

	public bool isRotating;
	public CaneControllerForSplash cane;
	public GameObject stand;
	public AudioClip rotateForSplashSound;

	void Start()
	{
		reset(transform.position.y);
	}


	public void reset(float y)
	{
		float posX = -Camera.main.orthographicSize * Camera.main.aspect + stand.GetComponent<BoxCollider2D>().size.x/2f;
		setPos (new Vector3(posX, y, transform.position.z));
		transform.eulerAngles = new Vector3 (0,0,0);
		//transform.Rotate (0, 0, 0);
	}

	private void setPos(Vector3 pos)
	{
		transform.position = pos;
		transform.eulerAngles = new Vector3 (0,0,0);
	}

	public void rotateForSplash()
	{
		SoundManager.instance.playSingle (rotateForSplashSound);
		isRotating = true;
		transform.Rotate (0, 0, 0);
	}

	void Update () 
	{
		if (isRotating) 
		{
			transform.Rotate(new Vector3 (0, 0, -400) * Time.deltaTime );
			if(transform.eulerAngles.z <= 230 )
			{ 
				transform.Rotate(new Vector3 (0, 0, 300) * Time.deltaTime );
				if (transform.eulerAngles.z <= 190)
				{
					isRotating = false;
					cane.checkAlive ();
				}

			}
		}
	}
}
