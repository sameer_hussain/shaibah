﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour 
{
	public AudioSource efxSource;                   //Drag a reference to the audio source which will play the sound effects.
	public AudioSource musicSource; 
	public AudioSource footstepsSource;   //Drag a reference to the audio source which will play the music.
	public static SoundManager instance = null;     //Allows other scripts to call functions from SoundManager.             
	public float lowPitchRange = .95f;              //The lowest a sound effect will be randomly pitched.
	public float highPitchRange = 1.05f;            //The highest a sound effect will be randomly pitched.
	public bool isSoundOn = true;

	void Awake ()
	{

		//Check if there is already an instance of SoundManager
		if (instance == null)
			//if not, set it to this.
			instance = this;
		//If instance already exists:
		else if (instance != this)
			//Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
			Destroy (gameObject);

		//Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
		DontDestroyOnLoad (gameObject);
	}

/*	public void enableSound()
	{
		GameManager.GetInstance().soundOn = true;
	}*/

	public void disableSound()
	{
		efxSource.volume = 0f;
		musicSource.volume = 0f;
		footstepsSource.volume = 0f;
		isSoundOn = false;
	}

	public void enableSound()
	{
		efxSource.volume = 1f;
		musicSource.volume = 1f;
		footstepsSource.volume = 1f;
		isSoundOn = true;
	}


	//Used to play single sound clips.
	public void playSingle(AudioClip clip)
	{
		efxSource.clip = clip;
		efxSource.Play ();
	}
		

	public void playFootsteps (params AudioClip[] clips)
	{
		int randomIndex = Random.Range(0, clips.Length);
		float randomPitch = Random.Range(lowPitchRange, highPitchRange);
		footstepsSource.pitch = randomPitch;
		footstepsSource.clip = clips[randomIndex];
		footstepsSource.Play();
	}
}
