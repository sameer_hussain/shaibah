﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Prime31;
using ArabicSupport;

public class TitleUI : MonoBehaviour 
{
	public Button playButton;
	public Button playSplashButton;
	public Button soundOn;
	public Button soundOff;
	public GameObject bottleShopUI;
	public GameObject characterShopUI;
	public GameObject noAdsButton;
	public Image characterImage;
	public Image fadeImage;

	private bool isInTransition;
	private float transition;
	private bool isShowing;
	private float duration;

	private int gameSelected;

	void Start ()
	{
		//PlayerPrefs.DeleteAll ();
		//PlayerPrefs.SetInt ("CherriesScore", 7000);
		PluginHelper_Analytics.startSession ("ZM32SNH4T2MZ3CBDCGFX",true);
		PluginHelper_Core.productListReceivedEvent += ProductListReceivedHandler;
		PluginHelper_Core.purchaseConsumableProductStatusEvent += purchaseConsumable;
		PluginHelper_Core.purchaseNonConsumableProductStatusEvent += purchaseNonConsumable;

		PluginHelper_Core.authenticate(); //GameCenterAuth

		playButton.onClick.AddListener(playGame);
		playButton.transform.Find ("Text").GetComponent<Text> ().text = ArabicFixer.Fix ("العب");

		playSplashButton.onClick.AddListener (playSplashGame);
		isInTransition = false;
		fadeImage.gameObject.SetActive( false);

		string[] productIdsIOS = new string[] {"bottle400", "bottle1000", "bottles3000", "bottles5000", "no_Ads"};
		string[] productIdsAndroid = new string[] {"bottle400", "bottle1000", "bottles3000", "bottles5000", "no_Ads"};
		PluginHelper_Core.requestProductData(productIdsIOS, productIdsAndroid);
		if (SoundManager.instance.isSoundOn) 
		{
			soundOn.gameObject.SetActive (true);
			soundOff.gameObject.SetActive (false);
		}
		else
		{
			soundOn.gameObject.SetActive (false);
			soundOff.gameObject.SetActive (true);
		}

		if (PlayerPrefs.HasKey ("CharacterSelected")) 
		{
			Debug.Log (PlayerPrefs.GetInt("CharacterSelected"));
		} 
		else 
		{
			PlayerPrefs.SetInt("CharacterSelected", 0);
		}

		if (PlayerPrefs.HasKey ("canShowAds")) 
		{
			Debug.Log (PlayerPrefs.GetString("canShowAds"));
			if (PlayerPrefs.GetString ("canShowAds") == "No")
			{
				noAdsButton.SetActive (false);
			}
		} 
		else 
		{
			PlayerPrefs.SetString("canShowAds", "Yes");
		}

		//charcterImage.sprite = 
	}

	void ProductListReceivedHandler(List<IAPProduct> pList)
	{
		Debug.Log ("ProductListReceivedHandler");
		Utils.logObject(pList);
	}


	public void noAds()
	{
		PluginHelper_Core.purchaseNonconsumableProduct ("no_Ads");
	}

	void purchaseNonConsumable(string error , bool didsuceed, string productID)
	{
		if(!didsuceed)
		{
			Debug.Log ("sameer" + error);
		
		}
		else 
		{
			Debug.Log ("Product Id" + productID);
			if (productID == "no_Ads")
			{
				PlayerPrefs.SetString("canShowAds", "No");
				noAdsButton.SetActive (false);
				Debug.Log ("No Ads");
			}
		}
	}
		
	void purchaseConsumable(string error, bool didsucceed, string productID)
	{
		if(!didsucceed)
		{
			Debug.Log ("sameer" + error);
		}
		else 
		{
			if (productID == "bottle400")
			{
				int cherriesScore = PlayerPrefs.GetInt ("CherriesScore");
				cherriesScore = cherriesScore + 400;
				PlayerPrefs.SetInt ("CherriesScore", cherriesScore);
			}
			else if (productID == "bottle1000")
			{
				int cherriesScore = PlayerPrefs.GetInt ("CherriesScore");
				cherriesScore = cherriesScore + 1000;
				PlayerPrefs.SetInt ("CherriesScore", cherriesScore);
			}
			else if (productID == "bottles3000")
			{
				int cherriesScore = PlayerPrefs.GetInt ("CherriesScore");
				cherriesScore = cherriesScore + 3000;
				PlayerPrefs.SetInt ("CherriesScore", cherriesScore);
			}	
			else if (productID == "bottles5000")
			{
				int cherriesScore = PlayerPrefs.GetInt ("CherriesScore");
				cherriesScore = cherriesScore + 5000;
				PlayerPrefs.SetInt ("CherriesScore", cherriesScore);
			}
		}
	}

	public void playGame()
	{
		gameSelected = 1;
		fade(true, 1.25f);
		//
	}

	public void playSplashGame()
	{
		gameSelected = 2;
		fade(true, 1.25f);
	}


	public void fade(bool showing, float duration)
	{
		fadeImage.gameObject.SetActive( true);
		Debug.Log ("fade");
		isShowing = showing;
		isInTransition = true;
		this.duration = duration;
		transition = (isShowing) ? 0 : 1;
	}

	public void bottleShopButtonPressed()
	{
		bottleShopUI.SetActive (true);
		bottleShopUI.GetComponent<BottleShop> ().showBottleShop ();
	}

	public void characterShopButtonPressed()
	{
		characterShopUI.SetActive (true);
		characterShopUI.GetComponent<CharacterShop> ().showCharacterShop ();
		Debug.Log ("show character shop");
	}

	public void disableSoundButtonPressed()
	{
		soundOff.gameObject.SetActive (true);
		soundOn.gameObject.SetActive (false);
		SoundManager.instance.disableSound ();	
	}

	public void enableSoundButtonPressed()
	{
		soundOff.gameObject.SetActive (false);
		soundOn.gameObject.SetActive (true);
		SoundManager.instance.enableSound ();	
	}

	private void Update()
	{
		int x = PlayerPrefs.GetInt("CharacterSelected") + 1;
		string characterSpriteName = "Character" + x.ToString() + "_Idle_1";
		characterImage.sprite = Resources.Load<Sprite> (characterSpriteName) as Sprite;
		if (!isInTransition) 
		{
			//fadeImage.gameObject.SetActive( false);
			return;
		}
		transition += (isShowing) ? Time.deltaTime * (1/duration) : -Time.deltaTime * (1/duration);
		fadeImage.color = Color.Lerp (new Color(0,0,0,0), new Color(0,0,0,1), transition);

		if (transition > 1 || transition < 0) 
		{
			if (gameSelected == 1) 
			{
				Application.LoadLevel("Game 1");
			}
			else if (gameSelected == 2) 
			{
				Application.LoadLevel("Game 2");
			}

			isInTransition = false;
		}

	}

}
