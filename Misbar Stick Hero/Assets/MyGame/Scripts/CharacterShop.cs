﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using ArabicSupport;

public class CharacterShop : MonoBehaviour 
{
	public Text bottleCount; 
	public GameObject [] playerButtons;
	public Text heroesText;
	private float screenHeight;
	private float screenWidth;
	private bool moveToTop;
	private bool moveToBottom;



	// Use this for initialization
	void Start () 
	{
		//PlayerPrefs.DeleteAll ();
		//PlayerPrefs.SetInt ("CherriesScore", 80);
		//moveToTop = false;
		heroesText.text = ArabicFixer.Fix("الشخصيات");
		screenHeight = Camera.main.orthographicSize * 2;
		screenWidth = screenHeight * Camera.main.aspect;
		transform.position = new Vector3 (transform.position.x, -screenHeight, transform.position.z);

		if (PlayerPrefs.HasKey ("CharactersUnlocked")) 
		{
			string characters = PlayerPrefs.GetString ("CharactersUnlocked");
			Debug.Log (characters);
			string[] charactersArray = characters.Split ('%');
			for (int i = 0; i < charactersArray.Length - 1; i++) 
			{
				Debug.Log (charactersArray [i]);
				for (int j =0; j < 23; j++)
				{
					if (j == int.Parse(charactersArray [i]))
					{
						playerButtons [j].GetComponent<HeroSelectionButton> ().unlock ();
						//playerButtons [j].GetComponent<HeroSelectionButton> ().isUnlocked = true;
					}

				}
			}
		}
		else
		{
			Debug.Log ("First Launch");
			PlayerPrefs.SetString("CharactersUnlocked", "0%");
		}
	}
		
	public void showCharacterShop()
	{
		moveToBottom = false;
		moveToTop = true;
	}

	public void hideCharacterShop()
	{
		moveToBottom = true;
	}
	// Update is called once per frame
	void Update () 
	{
		bottleCount.text = PlayerPrefs.GetInt ("CherriesScore").ToString();
		if (moveToTop) 
		{
			
			Vector3 targetA = new Vector3 (transform.position.x, 0, transform.position.z);
			transform.position = Vector3.MoveTowards (transform.position, targetA, 30f * Time.deltaTime);
			if (transform.position == targetA) 
			{
				moveToTop = false;
			}
		}

		if (moveToBottom) 
		{
			Vector3 target = new Vector3 (transform.position.x, -screenHeight, transform.position.z);
			transform.position = Vector3.MoveTowards (transform.position, target, 30f * Time.deltaTime);
			if (transform.position == target) 
			{
				moveToBottom = false;
				gameObject.SetActive (false);
			}
		}
	}
}
