﻿using UnityEngine;
using System.Collections;

public class SceneSwitcher : MonoBehaviour {

	public void loadFlurryScene()
	{
		Application.LoadLevel("FlurryAndroidTestScene");
	}

	public void loadPlayServices()
	{
		Application.LoadLevel("PlayGameServicesDemoScene");	
	}
	public void loadAdmob()
	{
		Application.LoadLevel("AdMobTestScene");	
	}
	public void loadinAppCombo()
	{
		Application.LoadLevel("IAPCombo");	
	}

	public void loadChartboost()
	{
		Application.LoadLevel("ChartboostExample");	
	}


}
