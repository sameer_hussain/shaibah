﻿using UnityEngine;
using System.Collections;

public class GAUIManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public  void initializeGAObject()
	{
		GAnalyticsDemo.initializeGAObject();
	}
	
	
	
	public    void startSession()
	{
		this.initializeGAObject();
		GAnalyticsDemo.startSession();
	}
	
	public   void dispatchHits()
	{
		GAnalyticsDemo.dispatchHits();
	}
	
	public   void logScreen()
	{

		GAnalyticsDemo.logScreen();

	}
	
	public   void logEvent()
	{
		GAnalyticsDemo.logEvent();
	
	}
	
	
	public   void logException()
	{
		GAnalyticsDemo.logException();
	}
	
	
	public   void logTiming()
	{
		GAnalyticsDemo.logTiming();
	}
	
	public   void logSocial()
	{
		GAnalyticsDemo.logSocial();
	}


}
