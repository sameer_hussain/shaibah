﻿using UnityEngine;
using System.Collections;

public static  class GAnalyticsDemo {


	public static GoogleAnalyticsV4 googleAnalytics;


	public static void initializeGAObject()
	{
		googleAnalytics = GoogleAnalyticsV4.getInstance();
	}



	public static  void startSession()
	{
		googleAnalytics.StartSession();
	}

	public static void dispatchHits()
	{
		googleAnalytics.DispatchHits();
	}

	public static void logScreen()
	{
		googleAnalytics.LogScreen("Main Menu");
		
		//Builder Hit with all App View parameters (all parameters required):
		googleAnalytics.LogScreen(new AppViewHitBuilder()
		                          .SetScreenName("Main Menu"));
	}

	public static void logEvent()
	{
		googleAnalytics.LogEvent("Achievement", "Unlocked", "Slay 10 dragons", 5);
		
		// Builder Hit with all Event parameters.
		googleAnalytics.LogEvent(new EventHitBuilder()
		                         .SetEventCategory("Achievement WP")
		                         .SetEventAction("Unlocked WP")
		                         .SetEventLabel("Slay 10 dragons WP")
		                         .SetEventValue(5));
		
		// Builder Hit with minimum required Event parameters.
		googleAnalytics.LogEvent(new EventHitBuilder()
		                         .SetEventCategory("Achievement RP")
		                         .SetEventAction("Unlocked RP"));
	}


	public static void logException()
	{
		googleAnalytics.LogException("Incorrect input exception", true);
		
		// Builder Hit with all Exception parameters.
		googleAnalytics.LogException(new ExceptionHitBuilder()
		                             .SetExceptionDescription("Incorrect input exception WP")
		                             .SetFatal(true));
		
		// Builder Hit with minimum required Exception parameters.
		googleAnalytics.LogException(new ExceptionHitBuilder());
	}


	public static void logTiming()
	{
		googleAnalytics.LogTiming("Loading", 50L, "Main Menu", "First Load");
		
		// Builder Hit with all Timing parameters.
		googleAnalytics.LogTiming(new TimingHitBuilder()
		                          .SetTimingCategory("Loading WP")
		                          .SetTimingInterval(50L)
		                          .SetTimingName("Main Menu WP")
		                          .SetTimingLabel("First load WP"));
		
		// Builder Hit with minimum required Timing parameters.
		googleAnalytics.LogTiming(new TimingHitBuilder()
		                          .SetTimingCategory("Loading RP")
		                          .SetTimingInterval(50L));
	}

	public static void logSocial()
	{
		googleAnalytics.LogSocial("twitter", "retweet", "twitter.com/googleanalytics/status/482210840234295296");
		
		// Builder Hit with all Social parameters.
		googleAnalytics.LogSocial(new SocialHitBuilder()
		                          .SetSocialNetwork("Twitter")
		                          .SetSocialAction("Retweet")
		                          .SetSocialTarget("twitter.com/googleanalytics/status/482210840234295296"));
		
		// Builder Hit with minimum required Social parameters.
		googleAnalytics.LogSocial(new SocialHitBuilder()
		                          .SetSocialNetwork("Twitter")
		                          .SetSocialAction("Retweet"));
	}


}
