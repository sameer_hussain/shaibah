﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Prime31;

public class DemoScenePluginHelperScript : MonoBehaviour {



	bool [] tog= new bool[5];
	void Start () {

		PluginHelper_Core.productListReceivedEvent += ProductListReceivedHandler;
		tog = new bool[]{false,false,false,false,true};

	}

	void ProductListReceivedHandler(List<IAPProduct> pList)
	{

		Debug.Log ("ProductListReceivedHandler");
		Utils.logObject(pList);
	}
	// Update is called once per frame
	void Update () 
	{
		ButtonHeight = Screen.height/5;
		ButtonWidth =  Screen.width/2-20;
	}
	
	float ButtonHeight = Screen.height/5;
	float ButtonWidth = Screen.width/2-20;
	void OnGUI ()
	{




		GUI.color = Color.black;
		float ButtonPosx = 10;
		float ButtonPosy = 10;
		GUI.color = Color.white;
//		if(GUI.Button(new Rect(ButtonPosx,ButtonPosy,ButtonWidth,ButtonHeight),""))
//		{
//
//		}


		ButtonPosx += ButtonWidth+10;
		GUIStyle TEXT = new GUIStyle() ;
		TEXT.alignment = TextAnchor.MiddleCenter;
		TEXT.fontStyle = FontStyle.Bold;
		TEXT.fontSize = 30;


		GUI.TextField(new Rect(0,ButtonPosy,Screen.width,ButtonHeight/2),"IAP Combo Demo Scene", TEXT);
		
		
		ButtonPosx -= ButtonWidth+10;
		ButtonPosy += ButtonHeight+30;
		if(GUI.Button(new Rect(ButtonPosx,ButtonPosy,ButtonWidth,ButtonHeight),"init(AndroidPublicKey)"))
		{
			var key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAg3lZQ57xdXtxy3GKtfSepg6fQzEetNKAs/+wMKentWTq+yx5X8uXe4jZ3cM8EIeVvysLDJ9w93N1XT7vyXpi4tOEAD9qxx5TZBVtiBxpAOlg1i72ZFIqQ9wkwGTv4vwxzY58vtJKtl4sJtFrCkjPOsthHoUKBwmXddWYXP0+r+QbyB8JYG8qReNJscj2cRutTyx8AlxT7k82txywTZzNUjcAn6ZqqaSls2t4hBTOxsvpI7Ww80IuzBXdigO89/NQ+0kVz0+G264uXO2LWW4xcvmbZHf5l3UPv1CbkPDw8WGdmyHrn1a+tQk8hG0Rm1Cg0TZ36sawr2ihibPHLhRQDwIDAQAB";
			PluginHelper_Core.init(key);
		}
		
		ButtonPosx += ButtonWidth+10;
		if(GUI.Button(new Rect(ButtonPosx,ButtonPosy,ButtonWidth,ButtonHeight),"RequestProductData"))
		{
			var androidSkus = new string[] { "testproduct1", "testproduct2", "testproduct3_typesubscription" };
			var iosProductIds = new string[] { "star_pack1", "stars_doubler_pack", "star_pack5", "star_pack2","star_pack3","star_pack4" };

			PluginHelper_Core.requestProductData(iosProductIds,androidSkus);

		}
		
		ButtonPosx -= ButtonWidth+10;
		ButtonPosy += ButtonHeight+30;
		if(GUI.Button(new Rect(ButtonPosx,ButtonPosy,ButtonWidth,ButtonHeight),"purchaseNonconsumableProduct"))
		{
			#if UNITY_ANDROID
			var productId = "testproduct1";
			#elif UNITY_IOS
			var productId = "stars_doubler_pack";
			#endif
			PluginHelper_Core.purchaseNonconsumableProduct(productId);

		}
		ButtonPosx += ButtonWidth+10;
		if(GUI.Button(new Rect(ButtonPosx,ButtonPosy,ButtonWidth,ButtonHeight),"purchaseConsumableProduct"))
		{
			#if UNITY_ANDROID
			var productId = "testproduct2";
			#elif UNITY_IOS
			var productId = "star_pack1";
			#endif
			PluginHelper_Core.purchaseConsumableProduct(productId);
		}
		ButtonPosx -= ButtonWidth+10;
		ButtonPosy += ButtonHeight+30;
		if (GUI.Button (new Rect (ButtonPosx, ButtonPosy, ButtonWidth, ButtonHeight),"restoreCompletedTransactions(iOS Only)" )) 
		{
				
		}
		ButtonPosx += ButtonWidth+10;
		if(GUI.Button(new Rect(ButtonPosx,ButtonPosy,ButtonWidth,ButtonHeight),"Load Scene GameCenter/GPGS"))
		{
						Application.LoadLevel("GPGSandGCScenePluginHelper");
		}
	}

	public void HandleproductListEvent (IAPProduct obj)
	{
		Debug.Log("HandleproductListEvent");
		Utils.logObject(obj);
	}
}
