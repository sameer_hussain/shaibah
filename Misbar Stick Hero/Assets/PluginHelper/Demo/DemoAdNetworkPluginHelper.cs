﻿using UnityEngine;
using System.Collections;
using ChartboostSDK;
using UnityEngine.Advertisements;

public class DemoAdNetworkPluginHelper : MonoBehaviour {


	public string zoneId;

	void ShowAdPlacement ()
	{
		if (string.IsNullOrEmpty (zoneId)) zoneId = null;

		ShowOptions options = new ShowOptions();
		options.resultCallback = HandleShowResult;

		Advertisement.Show (zoneId, options);
	}

	private void HandleShowResult (ShowResult result)
	{
		switch (result) {
		case ShowResult.Finished:
			Debug.Log ("Video completed. Offer a reward to the player.");
			break;
		case ShowResult.Skipped:
			Debug.LogWarning ("Video was skipped.");
			break;
		case ShowResult.Failed:
			Debug.LogError ("Video failed to show.");
			break;
		}
	}



	public void CB()
	{
		Debug.Log("Switched to CB");
//				PluginHelper_Ads._activStaticAd = StaticAdNetworkSelection.ChartBoost;
//				PluginHelper_Ads._activeVideoAd = VideoAdNetworkSelection.ChartBoost;
		PluginHelper_Ads._activStaticAd = StaticAdNetworkSelection.unity_ads;

	}

	public void AL()
	{
		Debug.Log("Switched to AL");
		PluginHelper_Ads.initApplovin();

		PluginHelper_Ads._activStaticAd = StaticAdNetworkSelection.applovin;
		PluginHelper_Ads._activeVideoAd = VideoAdNetworkSelection.applovin;
	}

	public void AM()
	{
		Debug.Log("Switched to AM");

		PluginHelper_Ads._activStaticAd = StaticAdNetworkSelection.adcolony;
		PluginHelper_Ads._activeVideoAd = VideoAdNetworkSelection.adcolony;
	}


	public void cacheInterstitial()
	{
		PluginHelper_Ads.setTestDevices(new string[] {"CF72F5AD0BBBA783F51489CCE1CE3FF7"});
		PluginHelper_Ads.cacheStaticAd("Default","ca-app-pub-8386987260001674/9875638345");
	}

	public void showInterstitial()
	{
		PluginHelper_Ads.showStaticAd("Default");
	}

#if UNITY_ANDROID
	public void createBanner()
	{
		PluginHelper_Ads.createBanner( "ca-app-pub-8386987260001674/8398905145", AdMobAndroidAd.smartBanner, AdMobAdPlacement.BottomCenter );
	}
#endif
	public void cacheVideoAd()
	{
		string[] zoneIDs = new string[15];
		zoneIDs[0] = "String";
		PluginHelper_Ads.cacheVideoAd("Default","","",new string[]{""});
	}

	public void showVideoAd()
	{
		PluginHelper_Ads.showVideoAd("Default","adColonyzoneID");
	}

}
