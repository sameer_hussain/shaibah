﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class GPGSandGCScenePluginHelper : Prime31.MonoBehaviourGUI{
	

	void Start()
	{
		//PluginHelper_Core.authenticate();
	}
	
	
	/// <summary>
	/// due to the large number of buttons in this demo all sections of buttons are in their own method below for easier readability
	/// </summary>
	void OnGUI()
	{
		GUI.skin.label.alignment = TextAnchor.MiddleCenter;
		beginColumn();
		
		// toggle to show two different sets of buttons
		if( toggleButtonState( "Achievements/Events/Quests/Snapshots" ) )
			showAuthAndSetttingsButtons();
		else
			achievementsEventsAndQuestsButtons();
		toggleButton( "Achievements/Events/Quests/Snapshots", "Toggle Buttons" );
		
		
		endColumn( true );
		
		leaderboardsAndAchievementsButtons();
		
		endColumn( false );
		
		

	}
	
	

	
	private void showAuthAndSetttingsButtons()
	{
		GUILayout.Label( "Authentication and Settings" );
		

		if( GUILayout.Button( "Authenticate Silently (with no UI)" ) )
		{

		}
		
		
		if( GUILayout.Button( "Get Auth Token" ) )
		{
			var token = PluginHelper_Core.getAuthToken(null);
			Debug.Log( "token: " + token );
		}

		
		
		if( GUILayout.Button( "Authenticate" ) )
		{
			PluginHelper_Core.authenticate();
		}
		
		
		if( GUILayout.Button( "Sign Out" ) )
		{
			PluginHelper_Core.signOut();
		}
		
		
		if( GUILayout.Button( "Is Signed In" ) )
		{
			// Please note that the isSignedIn method is a total hack that was added to work around a current bug where Google
			// does not properly notify an app that the user signed out
			Debug.Log( "is signed in? " + PluginHelper_Core.isSignedIn() );
		}
		
		
		if( GUILayout.Button( "Get Player Info" ) )
		{
			var playerInfo = PluginHelper_Core.getLocalPlayerInfo_alias();
			Prime31.Utils.logObject( playerInfo );
			
			// if we are on Android and have an avatar image available, lets download the profile pic
			if( Application.platform == RuntimePlatform.Android && playerInfo.avatarUri != null )
				PluginHelper_Core.loadProfileImage( playerInfo.avatarUri );
		}
		
		
		if( GUILayout.Button( "Load Remote Player Info" ) )
		{
			string[] playerIDs = {"110453866202127902712","",""};

			PluginHelper_Core.loadPlayersData( playerIDs );
		}
	}
	
	
	private void achievementsEventsAndQuestsButtons()
	{
		GUILayout.Label( "Achievements" );
		
		if( GUILayout.Button( "Show Achievements" ) )
		{
			PluginHelper_Core.showAchievements();
		}
		
		
		if( GUILayout.Button( "Increment Achievement" ) )
		{
			PluginHelper_Core.reportAchievement( "com.werplay.funnywings.trydona", 0 ,5 );
		}
		
		
		if( GUILayout.Button( "Unlock Achievement" ) )
		{
			PluginHelper_Core.reportAchievement( "com.werplay.funnywings.trydona",0,0 );
		}
		
		
		GUILayout.Label( "Events and Quests" );
		
		if( GUILayout.Button( "Load All Events" ) )
		{
			//PluginHelper_Core.loadAllEvents();
		}
		
		
		if( GUILayout.Button( "Increment Event" ) )
		{
			// you will need to use your own eventId for this to work
			//PlayGameServices.incrementEvent( "CgkI_-mLmdQEEAIQCg", 1 );
		}
		
		
		if( GUILayout.Button( "Show Quest List" ) )
		{
			//PlayGameServices.showQuestList();
		}
		
		
		if( GUILayout.Button( "Load All Quests" ) )
		{
//			PlayGameServices.loadAllQuests();
//			PlayGameServices.showStateChangedPopup( "CgkI_-mLmdQEEAIQDw" );
		}
		
		
		GUILayout.Label( "Snapshots" );
		
		if( GUILayout.Button( "Show Snapshot List" ) )
		{
//			PlayGameServices.showSnapshotList( 5, "Your Saved Games!", true, true );
		}
		
		
		if( GUILayout.Button( "Save Snapshot" ) )
		{
//			var data = System.Text.Encoding.UTF8.GetBytes( "my saved data" );
//			PlayGameServices.saveSnapshot( "snappy", true, data, "The description of the data" );
		}
		
		
		if( GUILayout.Button( "Load Snapshot" ) )
		{
//			PlayGameServices.loadSnapshot( "snappy" );
		}
		
		
		if( GUILayout.Button( "Delete Snapshot" ) )
		{
//			PlayGameServices.deleteSnapshot( "snappy" );
		}
	}
	
	
	private void leaderboardsAndAchievementsButtons()
	{
		GUILayout.Label( "Leaderboards" );
		
		if( GUILayout.Button( "Show Leaderboard" ) )
		{
			PluginHelper_Core.showLeaderboard( "com.werplay.starcatcher" );
		}
		
		
		if( GUILayout.Button( "Show All Leaderboards" ) )
		{
			PluginHelper_Core.showLeaderboards();
		}
		
		
		if( GUILayout.Button( "Submit Score" ) )
		{
			PluginHelper_Core.submitScore( "com.werplay.starcatcher", 566 );
		}
		
		
		if( GUILayout.Button( "Load Raw Score Data" ) )
		{
			PluginHelper_Core.loadScoresForLeaderboard( "com.werplay.starcatcher", Scope.AllTime, false, false );
		}
		
		
		if( GUILayout.Button( "Get Leaderboard Metadata" ) )
		{
//			var info = PluginHelper_Core.me();
//			Prime31.Utils.logObject( info );
		}
		
		
		if( GUILayout.Button( "Get Achievement Metadata" ) )
		{
			//var info = PluginHelper_Core.retrieveAchievementMetadata();
			//Prime31.Utils.logObject( info );
		}
		
		
		if( GUILayout.Button( "Reload All Metadata" ) )
		{
			PluginHelper_Core.reloadAchievementAndLeaderboardData();
		}
		
		
		if( GUILayout.Button( "Show Share Dialog" ) )
		{
			PluginHelper_Core.showShareDialog( "I LOVE this game!", "http://prime31.com" );
		}
	}
	

}