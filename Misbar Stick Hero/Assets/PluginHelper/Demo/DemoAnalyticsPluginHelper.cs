﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DemoAnalyticsPluginHelper : Prime31.MonoBehaviourGUI {


//	PluginHelper_Analytics instance;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		beginColumn();
		
		if( GUILayout.Button( "Start Flurry Session" ) )
		{
			// Optional information
			//PluginHelper_Analytics.setAge( 12 );
			//PluginHelper_Analytics.setGender( Prime31.FlurryGender.Male,"M" );

			string apiKeyDroid = "H7V9DQDGKBHTK8S7FTV3";
			string apiKeyiOS = "4DDPC3MJCHJ4FWZJF394";
			// replace with your Flurry Key!!!
			#if  UNITY_ANDROID
			instance.startSession( apiKeyDroid );
			#endif
			#if UNITY_IOS
			PluginHelper_Analytics.startSession( apiKeyiOS );
			#endif

		}
		
		
		if( GUILayout.Button( "Log Event" ) )
		{
			//instance.logEvent( "Stuff Happened", false );
		}
		
		
		if( GUILayout.Button( "Log Event with Params" ) )
		{
			var dict = new Dictionary<string,string>();
			dict.Add( "akey1", "value1" );
			dict.Add( "bkey2", "value2" );
			dict.Add( "ckey3", "value3" );
			dict.Add( "dkey4", "value4" );

			var dict2 = new Dictionary<string,object>();
			dict2.Add( "akey1", "value1" );
			dict2.Add( "bkey2", "value2" );
			dict2.Add( "ckey3", "value3" );
			dict2.Add( "dkey4", "value4" );
			// string eventName, Dictionary<string,string> parameters, bool isTimed, string catogory, string action, long value
			
			PluginHelper_Analytics.logEvent( "EventWithParams", dict, false,"TestEvent","TestAction",0,dict2 );
			PluginHelper_Analytics.logEvent("UnityCheck",dict,dict2);

		}
		
		
		if( GUILayout.Button( "Log Timed Event" ) )
		{
			PluginHelper_Analytics.logEvent( "Timed Event", true );
		}
		
		
		if( GUILayout.Button( "End Timed Event" ) )
		{
			PluginHelper_Analytics.endTimedEvent( "Timed Event" );
		}
		
		
		if( GUILayout.Button( "Set Reports on Close" ) )
		{
			PluginHelper_Analytics.setSessionReportsOnCloseEnabled( true );
		}
		
		
		if( GUILayout.Button( "Set Reports on Pause" ) )
		{
			PluginHelper_Analytics.setSessionReportsOnPauseEnabled( true );
		}
		
		
		endColumn( true );
		
		

		endColumn();
	}

}
