﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using ChartboostSDK;
using UnityEngine.EventSystems;
using Prime31;
using UnityEngine.Advertisements;
 

public enum StaticAdNetworkSelection
{
	ChartBoost,
	vungle,
	applovin,
	adcolony,
	unity_ads
};
public enum VideoAdNetworkSelection
{
	ChartBoost,
	vungle,
	applovin,
	adcolony,
	unity_ads
};

public enum AdMobAdPlacement
{
	TopLeft,
	TopCenter,
	TopRight,
	Centered,
	BottomLeft,
	BottomCenter,
	BottomRight
}

public enum AdMobAndroidAd
{
	phone320x50,
	tablet300x250,
	tablet468x60,
	tablet728x90,
	smartBanner
}



public class PluginHelper_Ads : MonoBehaviour {

	public  VideoAdNetworkSelection activeVideoAd ;
	
	public  StaticAdNetworkSelection activStaticAd ; 

	public static VideoAdNetworkSelection _activeVideoAd;

	public  static StaticAdNetworkSelection _activStaticAd;

	public static event Action<CBLocation> didCacheStatic;



	// Use this for initialization
	void Start () 
	{

		_activeVideoAd = activeVideoAd;
		_activStaticAd = activStaticAd;
		
	}


	void init ()
	{
		Debug.Log ("Initialize SDK Applovin");
		AppLovin.InitializeSdk ();

	}

	
	// Update is called once per frame
	void Update () {
	
	}

	public static void initApplovin()
	{
		AppLovin.InitializeSdk ();
	}

	

	public static void showStaticAd(string location)
	{
		#if UNITY_ANDROID
		if(_activStaticAd == StaticAdNetworkSelection.ChartBoost )
		{
			Chartboost.showInterstitial(CBLocation.locationFromName(location));
		}
		else if(_activStaticAd == StaticAdNetworkSelection.vungle )
		{
			AdMobAndroid.displayInterstitial();
		}

		else  if(_activStaticAd == StaticAdNetworkSelection.applovin )
		{
			AppLovin.ShowInterstitial();
		}

//		else  if(_activStaticAd == StaticAdNetworkSelection.adcolony )
//		{
//
//
//		}



	    #endif

		#if UNITY_IOS

		if(_activStaticAd == StaticAdNetworkSelection.ChartBoost )
		{
			Chartboost.showInterstitial(CBLocation.locationFromName(location));	
		}
		else if(_activStaticAd == StaticAdNetworkSelection.vungle )
		{
			AdMobBinding.displayInterstitial();	
		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.applovin )
		{
			AppLovin.ShowInterstitial();
		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.unity_ads )
		{
			Advertisement.Show();
		}
		#endif

	}

	public static void showVideoAd(string location,string adColonyZoneID)
	{
		#if UNITY_ANDROID
		if(_activeVideoAd == VideoAdNetworkSelection.ChartBoost )
		{
			Chartboost.showRewardedVideo(CBLocation.locationFromName(location));
		}
		else if(_activeVideoAd == VideoAdNetworkSelection.vungle )
		{
			
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.applovin )
		{
			AppLovin.ShowInterstitial();
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.adcolony )
		{
			if(adColonyZoneID!=null)
			{
				AdColony.ShowVideoAd(adColonyZoneID);
			}
			else
				AdColony.ShowVideoAd();
		}
		#endif
		
		#if UNITY_IOS
		if(_activeVideoAd == VideoAdNetworkSelection.ChartBoost )
		{
			Chartboost.showRewardedVideo(CBLocation.locationFromName(location));
		}
		else if(_activeVideoAd == VideoAdNetworkSelection.vungle )
		{
			
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.applovin )
		{
			AppLovin.ShowInterstitial();
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.adcolony )
		{
			
		}
		#endif
		
	}

	public static void cacheStaticAd(string location, string adUnitID)
	{
		#if UNITY_ANDROID
		if(_activStaticAd == StaticAdNetworkSelection.ChartBoost )
		{
			Chartboost.cacheInterstitial(CBLocation.locationFromName(location));
		}
		else if(_activStaticAd == StaticAdNetworkSelection.adcolony)
		{
			
		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.applovin )
		{
			AppLovin.PreloadInterstitial();
		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.vungle )
		{
			AdMobAndroid.requestInterstitial( adUnitID );
		}
		#endif
		
		#if UNITY_IOS
		if(_activStaticAd == StaticAdNetworkSelection.ChartBoost )
		{
			Chartboost.cacheInterstitial(CBLocation.locationFromName(location));
		}
		else if(_activStaticAd == StaticAdNetworkSelection.vungle )
		{
			AdMobBinding.requestInterstitial(adUnitID);	
		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.applovin )
		{
			AppLovin.PreloadInterstitial();
		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.unity_ads )
		{
			Advertisement.Show();
		}
		#endif
	}

	public static void cacheVideoAd(string chartBoostLocation,string adcolonyAppVersion, string adcolonyAppID, string[] adcolonyZoneIDs )
	{
		#if UNITY_ANDROID
		if(_activeVideoAd == VideoAdNetworkSelection.ChartBoost )
		{
			Chartboost.cacheRewardedVideo(CBLocation.locationFromName(location));
		}
		else if(_activeVideoAd == VideoAdNetworkSelection.vungle )
		{
			
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.applovin )
		{
			AppLovin.IsPreloadedInterstitialVideo();
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.adcolony )
		{
			AdColony.Configure(appVersion,appID,zoneIDs);
		}
		#endif
		
		#if UNITY_IOS
		if(_activeVideoAd == VideoAdNetworkSelection.ChartBoost )
		{
			Chartboost.cacheRewardedVideo(CBLocation.locationFromName(chartBoostLocation));
		}
		else if(_activeVideoAd == VideoAdNetworkSelection.vungle )
		{
			
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.applovin )
		{
			AppLovin.IsPreloadedInterstitialVideo();
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.adcolony )
		{
			AdColony.Configure(adcolonyAppVersion,adcolonyAppID,adcolonyZoneIDs);

		}
		#endif
		
	}

	public static void showMoreApps(string location)
	{
		#if UNITY_ANDROID
		if(_activeVideoAd == VideoAdNetworkSelection.ChartBoost )
		{
			Chartboost.showMoreApps(CBLocation.locationFromName(location));
		}
		else if(_activeVideoAd == VideoAdNetworkSelection.vungle )
		{
			
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.applovin )
		{

		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.adcolony )
		{
			
		}
		#endif
		
		#if UNITY_IOS
		if(_activeVideoAd == VideoAdNetworkSelection.ChartBoost )
		{
			Chartboost.showMoreApps(CBLocation.locationFromName(location));
		}
		else if(_activeVideoAd == VideoAdNetworkSelection.vungle )
		{
			
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.applovin )
		{
			
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.adcolony )
		{
			
		}
		#endif
	}

	public static bool isStaticAdAvailbale(string location)
	{
		#if UNITY_ANDROID
		if(_activStaticAd == StaticAdNetworkSelection.ChartBoost )
		{
			return Chartboost.hasInterstitial(CBLocation.locationFromName(location));

		}
		else if(_activStaticAd == StaticAdNetworkSelection.vungle )
		{
			return AdMobAndroid.isInterstitialReady();
		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.applovin )
		{
			return  AppLovin.HasPreloadedInterstitial();
		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.adcolony )
		{

			return false;

		}
		#endif
		
		#if UNITY_IOS
		if(_activStaticAd == StaticAdNetworkSelection.ChartBoost )
		{
			return Chartboost.hasInterstitial(CBLocation.locationFromName(location));
		}
		else if(_activStaticAd == StaticAdNetworkSelection.vungle )
		{
			return false;
		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.applovin )
		{
			return AppLovin.HasPreloadedInterstitial();
		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.adcolony )
		{
			return AdMobBinding.isInterstitialAdReady();
		}
		#endif
		return false;
		
	}

	public static bool isVideoAdAvailbale(string location, string zoneID)
	{
		#if UNITY_ANDROID
		if(_activeVideoAd == VideoAdNetworkSelection.ChartBoost )
		{
			return Chartboost.hasRewardedVideo(CBLocation.locationFromName(location));
		}
		else if(_activeVideoAd == VideoAdNetworkSelection.vungle )
		{
			return false;
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.applovin )
		{
			return false;//AppLovin.HasPreloadedInterstitial();
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.adcolony )
		{

			return AdColony.IsVideoAvailable();
		}
		#endif
		
		#if UNITY_IOS
		if(_activeVideoAd == VideoAdNetworkSelection.ChartBoost )
		{
			return Chartboost.hasRewardedVideo(CBLocation.locationFromName(location));
		}
		else if(_activeVideoAd == VideoAdNetworkSelection.vungle )
		{
			return false;
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.applovin )
		{
			return false;
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.adcolony )
		{
			return AdColony.IsVideoAvailable(zoneID);
		}
		#endif
		return false;
	}

	public static bool iscacheStaticAdAvailbale( Action <CBLocation> Handler )
	{
		#if UNITY_ANDROID
		if(_activStaticAd == StaticAdNetworkSelection.ChartBoost )
		{
			//Chartboost.didCacheInterstitial = Handler;
			return true;
		}
		else if(_activStaticAd == StaticAdNetworkSelection.vungle )
		{
			return true;
		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.applovin )
		{
			return true;
		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.adcolony )
		{
			return true;
		}
		#endif
		return false;

		#if UNITY_IOS
		if(_activStaticAd == StaticAdNetworkSelection.ChartBoost )
		{
			//Chartboost.didCacheInterstitial = Handler;
		}
		else if(_activStaticAd == StaticAdNetworkSelection.vungle )
		{

		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.applovin )
		{

		}
		
		else  if(_activStaticAd == StaticAdNetworkSelection.adcolony )
		{

		}
		#endif
		return false;
		
	}

	public static bool iscacheVideoAdAvailable(Action <CBLocation> Handler)
	{
		#if UNITY_ANDROID
		if(_activeVideoAd == VideoAdNetworkSelection.ChartBoost )
		{
			Chartboost.didCacheRewardedVideo += Handler;
			return true;
		}
		else if(_activeVideoAd == VideoAdNetworkSelection.vungle )
		{
			return false;
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.applovin )
		{
			return AppLovin.IsPreloadedInterstitialVideo();
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.adcolony )
		{

			return false;
		}
		#endif
		
		#if UNITY_IOS
		if(_activeVideoAd == VideoAdNetworkSelection.ChartBoost )
		{
			return false;
		}
		else if(_activeVideoAd == VideoAdNetworkSelection.vungle )
		{
			return false;
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.applovin )
		{
			return AppLovin.IsPreloadedInterstitialVideo();
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.adcolony )
		{
			return false;
		}
		#endif
		return false;
	}

	public static bool isMoreAppsAvailable(Action<CBLocation> handler)
	{
		#if UNITY_ANDROID
		if(_activeVideoAd == VideoAdNetworkSelection.ChartBoost )
		{
//			Chartboost.didClickMoreApps = handler;
			return false;
		}
		else if(_activeVideoAd == VideoAdNetworkSelection.vungle )
		{
			return false;
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.applovin )
		{
			return false;
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.adcolony )
		{
			return false;
		}
		#endif
		
		#if UNITY_IOS
		if(_activeVideoAd == VideoAdNetworkSelection.ChartBoost )
		{
			return false;
		}
		else if(_activeVideoAd == VideoAdNetworkSelection.vungle )
		{
			return false;
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.applovin )
		{
			return false;
		}
		
		else  if(_activeVideoAd == VideoAdNetworkSelection.adcolony )
		{
			return false;
		}
		#endif
		return false;
	}

	// Sets test devices. This needs to be set BEFORE a banner is created. Watch the logcat logs to see your device ID while testing.
	public static void setTestDevices( string[] testDevices )
	{
		#if UNITY_ANDROID
		AdMobAndroid.setTestDevices(testDevices);
		#elif UNITY_IOS
		AdMobBinding.setTestDevices(testDevices);
		#endif
	}
	

	// Creates a banner of the given type at the given position. This method requires an adUnitId and you must be updated to the new AdMob system.
	public static void createBanner( string adUnitId, AdMobAndroidAd type,AdMobBannerType typeIOS, AdMobAdPlacement placement, AdMobAdPosition placementIOS )
	{
		#if UNITY_ANDROID
		AdMobAndroid.createBanner(adUnitId,type,placement);
		#elif UNITY_IOS
		AdMobBinding.createBanner(adUnitId,typeIOS,placementIOS);
		#endif
	}

	
	// Creates a banner of the given type at the given position. This method requires an adUnitId and you must be updated to the new AdMob system.

	public static void createBanner( string adUnitId,  int type, int placement )
	{
		#if UNITY_ANDROID
		AdMobAndroid.createBanner(adUnitId,type,placement);
		#elif UNITY_IOS
		AdMobBinding.createBanner(adUnitId,(AdMobBannerType)type, (AdMobAdPosition) placement); 
		#endif
	}

	
	
	// Destroys the banner if it is showing
	public static void destroyBanner()
	{
		#if UNITY_ANDROID
		AdMobAndroid.destroyBanner();
		#elif UNITY_IOS
		AdMobBinding.destroyBanner();
		#endif
	}
	
	
	// Hides/shows the ad banner
	public static void hideBanner( bool shouldHide )
	{
		#if UNITY_ANDROID
		AdMobAndroid.hideBanner(shouldHide);
		#elif UNITY_IOS
		AdMobBinding.hideBanner(shouldHide);
		#endif
	}
	
	
	// Refreshes the banner with a new ad request
	public static void refreshAd()
	{
		#if UNITY_ANDROID
		AdMobAndroid.refreshAd();
		#elif UNITY_IOS

		#endif
	}
	
	#if UNITY_ANDROID
	// Gets the height of the current ad view
	public static float getAdViewHeight()
	{
		#if UNITY_ANDROID
		return AdMobAndroid.getAdViewHeight();
		#elif UNITY_IOS
			
		#endif
	}
	#endif
	
	
	// Starts loading a reward based ad
	public static void requestRewardBasedAd( string adUnitId )
	{
		#if UNITY_ANDROID
		AdMobAndroid.requestRewardBasedAd(adUnitId);
		#elif UNITY_IOS
		
		#endif
	}
	
	
	// If a reward based ad is loaded this will take over the screen and show the ad
	public static void showRewardBasedAd()
	{
		#if UNITY_ANDROID
		AdMobAndroid.showRewardBasedAd();
		#elif UNITY_IOS
		
		#endif
	}




}
