﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Prime31;
using UnityEngine.Analytics;

public static  class PluginHelper_Analytics  {

	#if UNITY_IOS
	public enum FlurryGender
	{
		Male,
		Female,
		Unknown
	}
	#endif

	public static  GoogleAnalyticsV4 googleAnalytics; 
	
	public static void initializeGAObject()
	{
		googleAnalytics= GoogleAnalyticsV4.getInstance();
	}
	// Starts up your Flurry session. Call on application startup. Passing true for enableLogging will enable verbose debug logs for Flurrys SDK
	public static   void startSession( string apiKey, bool enableLogging = false )
	{
		#if UNITY_ANDROID
		FlurryAnalytics.startSession(apiKey,enableLogging);
		googleAnalytics.StartSession();
		googleAnalytics.DispatchHits();

		#endif

		#if UNITY_IOS
		//googleAnalytics.StartSession();
		FlurryAnalytics.startSession(apiKey,enableLogging);
		#endif
	}
	
	
	// Ends your Flurry session. This is automatically called for you when your app is backgrounded so unless you have a good reason to call it you should never need to use it directly.
	public static  void onEndSession()
	{
		#if UNITY_ANDROID
		FlurryAnalytics.onEndSession();
		#endif
		
		#if UNITY_IOS
		googleAnalytics.StopSession();
		#endif

	}
	
	
	// Ads a user cookie
	public static  void addUserCookie( string key, string value )
	{
		#if UNITY_ANDROID
		FlurryAnalytics.addUserCookie(key,value);
		#endif
		
		#if UNITY_IOS

		#endif
	}
	
	
	// Clears all user cookies
	public static  void clearUserCookies()
	{
		#if UNITY_ANDROID
		FlurryAnalytics.clearUserCookies();
		#endif
		
		#if UNITY_IOS

		#endif

	}
	
	
	// Changes the window during which a session can be resumed.  Must be called before onStartSession!
	public static  void setContinueSessionMillis( long milliseconds )
	{
		#if UNITY_ANDROID
		FlurryAnalytics.setContinueSessionMillis(milliseconds);
		#endif
		
		#if UNITY_IOS

		#endif
	}
	
	
	// Logs an event to Flurry that is optionally timed
	public static  void logEvent( string eventName, bool isTimed )
	{
		#if UNITY_ANDROID
		if(isTimed)
			FlurryAnalytics.logEvent(eventName,isTimed);
		else
			FlurryAnalytics.logEvent(eventName,false);
		#endif
		
		#if UNITY_IOS
		if(isTimed)
			FlurryAnalytics.logEvent(eventName,isTimed);
			
		else
			FlurryAnalytics.logEvent(eventName,false);
		#endif
	}
	
	
	public static  void logEvent( string eventName)
	{
		#if UNITY_ANDROID
		FlurryAnalytics.logEvent(eventName,false);
		#endif
		
		#if UNITY_IOS
		FlurryAnalytics.logEvent(eventName,false);
		#endif
	}
	
	
	// Logs an event with optional key/value pairs that is optionally timed
	public static  void logEvent( string eventName, Dictionary<string,string> parameters, Dictionary<string,object> unity_parameters )
	{
		#if UNITY_ANDROID
		FlurryAnalytics.logEvent(eventName,parameters);
		Analytics.CustomEvent(eventName,unity_parameters);
		#endif
		#if UNITY_IOS
		FlurryAnalytics.logEventWithParameters(eventName,parameters,false);
		Analytics.CustomEvent(eventName,unity_parameters);
		#endif

	}
	
	
	public static  void logEvent( string eventName, Dictionary<string,string> parameters, bool isTimed, string catogory, string action, long value, Dictionary<string,object> unity_parameters )
	{
		#if UNITY_ANDROID
		if(eventName == null && catogory == null )
		{
			Debug.Log("To log event at google analytics you need to provide atleast  eventName and catogory");
		}
		else
		{
			googleAnalytics.LogEvent(catogory,action,eventName,value);
		}
		if(isTimed==false && parameters==null)
		{
			FlurryAnalytics.logEvent(eventName,false);
		}
		else if (isTimed==false && parameters!=null)
		{
			FlurryAnalytics.logEvent(eventName,parameters,false);
			Analytics.CustomEvent(eventName,unity_parameters);
		}
		else if(isTimed == true && parameters==null)
		{
			FlurryAnalytics.logEvent(eventName,isTimed);
		}
		else
		{
			FlurryAnalytics.logEvent(eventName,parameters,isTimed);
		}
		#endif
		
		#if UNITY_IOS

		if(eventName == null || catogory == null || action == null || value == 0 )
		{
			Debug.Log("To log event at google analytics you need to provide atleast one argument eventName, catogory, action, value");
		}
		else
		{
			googleAnalytics.LogEvent(catogory,action,eventName,value);
		}
		if(isTimed==false && parameters==null)
		{
			FlurryAnalytics.logEvent(eventName,false);

		}
		else if (isTimed==false && parameters!=null)
		{
			FlurryAnalytics.logEventWithParameters(eventName,parameters,false);
			Analytics.CustomEvent(eventName,unity_parameters);
		}
		else if(isTimed == true && parameters==null)
		{
			FlurryAnalytics.logEvent(eventName,isTimed);
		}
		else
		{
			FlurryAnalytics.logEventWithParameters(eventName,parameters,isTimed);
		}

		#endif
	}
	
	
	// Ends a timed event with optional key/value pairs
	public static  void endTimedEvent( string eventName )
	{
		#if UNITY_ANDROID
		FlurryAnalytics.endTimedEvent(eventName);
		#endif
		
		#if UNITY_IOS
		FlurryAnalytics.endTimedEvent(eventName);
		#endif
	}
	
	
	public static  void endTimedEvent( string eventName, Dictionary<string,string> parameters )
	{
		#if UNITY_ANDROID
		FlurryAnalytics.endTimedEvent(eventName,parameters);
		#endif
		
		#if UNITY_IOS
		FlurryAnalytics.endTimedEvent(eventName,parameters);
		#endif
	}
	
	
	// Use onPageView to report page view count
	public static  void onPageView()
	{
		#if UNITY_ANDROID
		FlurryAnalytics.onPageView();
		#endif
		
		#if UNITY_IOS

		#endif
	}
	
	
	// Use onError to report application errors
	public static  void onError( string errorId, string message, string errorClass )
	{
		#if UNITY_ANDROID
		FlurryAnalytics.onError(errorId,message,errorClass);
		#endif
		
		#if UNITY_IOS

		#endif
	}
	
	
	// Use this to log the user's assigned ID or username in your system
	public static  void setUserID( string userId )
	{
		#if UNITY_ANDROID
		FlurryAnalytics.setUserID(userId);
		#endif
		
		#if UNITY_IOS
		FlurryAnalytics.setUserId(userId);
		#endif
	}
	
	
	// Use this to log the user's age. Valid inputs are between 1 and 109.
	public static  void setAge( int age, int BirthYear )
	{
		#if UNITY_ANDROID
		FlurryAnalytics.setAge(age);
		Analytics.SetUserBirthYear(BirthYear);
		#endif
		
		#if UNITY_IOS
		FlurryAnalytics.setAge(age);
		Analytics.SetUserBirthYear(BirthYear);
		#endif
	}
	
	
	// Sends the user's gender to Flurrys servers.
	public static  void setGender( FlurryGender gender, string genderString, Gender unityGender )
	{
		#if UNITY_ANDROID
		FlurryAnalytics.setGender(gender);
		Analytics.SetUserGender(unityGender);
		#endif
		
		#if UNITY_IOS
		FlurryAnalytics.setGender(genderString);
		Analytics.SetUserGender(unityGender);
	
		#endif
	}
	
	
	// To enable/disable FlurryAgent logging call
	public static  void setLogEnabled( bool enable )
	{
		#if UNITY_ANDROID
		FlurryAnalytics.setLogEnabled(enable);
		#endif
		
		#if UNITY_IOS
		FlurryAnalytics.setDebugLogEnabled(enable);
		#endif
	}

	// Sets the app version for this build overriding the CFBundleVersion
	public static  void setAppVersion( string appVersion )
	{
		#if UNITY_IOS
		FlurryAnalytics.setAppVersion (appVersion);
		#endif
	}

	public static  void setSessionReportsOnCloseEnabled( bool sendSessionReportsOnClose )
		
	{
		#if UNITY_IOS
		FlurryAnalytics.setSessionReportsOnCloseEnabled (sendSessionReportsOnClose);
		#endif
	}
	
	

	
	// Sets whether Flurry should upload session reports when your app is paused
	public static  void setSessionReportsOnPauseEnabled( bool setSessionReportsOnPauseEnabled )
	{
		#if UNITY_IOS
		FlurryAnalytics.setSessionReportsOnPauseEnabled (setSessionReportsOnPauseEnabled);
		#endif
	}

	//Special Calls for Google Analytics

	public static  void LogException(string Description, bool isFatal)
	{
		#if UNITY_ANDROID
		googleAnalytics.LogException (Description,isFatal);
		#endif
		#if UNITY_IOS
		googleAnalytics.LogException (Description,isFatal);
		#endif
	}

	public static void LogScreen(string title)
	{
		#if UNITY_ANDROID
		googleAnalytics.LogScreen (title);
		#endif
		#if UNITY_IOS
		googleAnalytics.LogScreen (title);
		#endif
	}
	public static void LogTiming(string timingCategory, long timingInterval,string timingName,string timingLabel)
	{
		#if UNITY_ANDROID
		googleAnalytics.LogTiming (timingCategory,timingInterval,timingName,timingLabel);
		#endif
		#if UNITY_IOS
		googleAnalytics.LogTiming (timingCategory,timingInterval,timingName,timingLabel);
		#endif
	}
	public static void LogSocial(string socialNetwork,string socialAction,string socialTarget)
	{
		#if UNITY_ANDROID
		googleAnalytics.LogSocial (socialNetwork,socialAction,socialTarget);
		#endif
		#if UNITY_IOS
		googleAnalytics.LogSocial (socialNetwork,socialAction,socialTarget);
		#endif
	}
	public static void LogTransaction(string transID,string affiliation,double revenue,double tax,double shipping)
	{
		#if UNITY_ANDROID
		googleAnalytics.LogTransaction (transID,affiliation,revenue,tax,shipping);
		#endif
		#if UNITY_IOS
		googleAnalytics.LogTransaction (transID,affiliation,revenue,tax,shipping);
		#endif
	}
	public static void LogTransaction(string transID,string affiliation,double revenue,double tax,double shipping,string currencyCode)
	{
		#if UNITY_ANDROID
		googleAnalytics.LogTransaction(transID,affiliation,revenue,tax,shipping,currencyCode);
		#endif
		#if UNITY_IOS
		googleAnalytics.LogTransaction(transID,affiliation,revenue,tax,shipping,currencyCode);
		#endif
	}
	public static void LogItem(string transID,string name,string SKU,string category,double price,long quantity)
	{
		#if UNITY_ANDROID
		googleAnalytics.LogItem(transID,name,SKU,category,price,quantity);
		#endif
		#if UNITY_IOS
		googleAnalytics.LogItem(transID,name,SKU,category,price,quantity);
		#endif
		
	}
	public static void LogItem(string transID,string name,string SKU,string category,double price,long quantity,string currencyCode)
	{
		#if UNITY_ANDROID
		googleAnalytics.LogItem(transID,name,SKU,category,price,quantity,currencyCode);
		#endif
		#if UNITY_IOS
		googleAnalytics.LogItem(transID,name,SKU,category,price,quantity,currencyCode);
		#endif
	}




	
}
