﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Prime31;
using Batch;
public enum Scope
{
	Unknown = -1,
	Today = 0,         //iOS+1 Android enum
	Week =1 ,          //iOS +1 Android
	AllTime= 2			// iOS +1 Android
};


public class PluginHelper_Core : MonoBehaviour {



	public static Action<List<IAPProduct>> productListReceivedEvent; 
	public static Action <string, bool, string>purchaseConsumableProductStatusEvent;
	public static Action <string, bool, string>purchaseNonConsumableProductStatusEvent;
	//
	//Batch Declaration

	public static BatchPlugin BatchPlugin;

	//Batch Declaration End


	// Use this for initialization
	void Start ()
	{



	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	/////////////////////////////
	/// InApps calls Stareted////
	/////////////////////////////
	void testFunc(IAPProduct obj)
	{
		Debug.Log ("working");
	}

	public static void batchInit(string batchKey)
	{
		//Batch Initialization
		Batch.Config batch_config = new Batch.Config ();
		batch_config.IOSAPIKey = batchKey;
		BatchPlugin.Push.Setup ();

		BatchPlugin.StartPlugin (batch_config);
	
		// note that you should probably move this call to a more appropriate place
		// in the lifecycle of your app
		BatchPlugin.Push.RegisterForRemoteNotifications ();

	}
	//Batch Initialization

	
	//key from android portal 
	// Initializes the billing system. Call this at app launch to prepare the IAP system.
	public static void init(string androidPublicKey)
	{
		IAP.init( androidPublicKey );
		
	}
	// Accepts two arrays of product identifiers (one for iOS one for Android). All of the products you have for sale should be requested in one call.
	// On Android, after this call completes the androidPurchasedItems will be populated.	
	public static void requestProductData( string[] iosProductIdentifiers, string[] androidSkus)
	{
		IAP.requestProductData( iosProductIdentifiers, androidSkus, productList =>
		                       {
//			Debug.Log( "Product list received" );
//			Utils.logObject( productList );
			productListReceivedEvent(productList);
		});
	}
	// Purchases the given product and quantity. completionHandler provides if the purchase succeeded (bool) and an error message which will be populated if
	// the purchase failed.
	public static void purchaseConsumableProduct( string productId )
	{
		IAP.purchaseConsumableProduct( productId, ( didSucceed, error ) =>
		                              {

			Debug.Log( "purchasing product " + productId + " result: " + didSucceed );
			
			if( !didSucceed )
				Debug.Log( "purchase error: " + error );

				purchaseConsumableProductStatusEvent(error,didSucceed,productId);
		});
		
	}
	// Purchases the given product and quantity. completionHandler provides if the purchase succeeded (bool) and an error message which will be populated if
	// the purchase failed.
	public static void purchaseNonconsumableProduct( string productId)
	{
		IAP.purchaseNonconsumableProduct( productId, ( didSucceed, error ) =>
		                                 {
			Debug.Log( "purchasing product " + productId + " result: " + didSucceed );
			
			if( !didSucceed )
				Debug.Log( "purchase error: " + error );

				purchaseNonConsumableProductStatusEvent(error,didSucceed,productId);
		});
		
	}
	// iOS Only. Restores all previous transactions. This is used when a user gets a new device and they need to restore their old purchases.
	// DO NOT call this on every launch. It will prompt the user for their password. Each transaction that is restored will have
	// the completion handler called for it
	public static void restoreCompletedTransactions( Action<string> completionHandler )
	{
		IAP.restoreCompletedTransactions (completionHandler);
	}
	
	
	//////////////////////////////
	/// InApps calls Completed////
	//////////////////////////////
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
	
	
	//////////////////////////////////////////
	/// Game Center and GPGS Calls Started////
	//////////////////////////////////////////

	///Authentications And Settings///
	//////////////////////////////////

	//Silent Authentication
	public static void attemptSilentAuthentication()
	{

		#if UNITY_ANDROID
		PlayGameServices.attemptSilentAuthentication();
		#endif
	}

	//Get Authentication Token
	public static string getAuthToken( string scope )
	{

		#if UNITY_ANDROID
		return PlayGameServices.getAuthToken(scope);
		#endif

		#if UNITY_IOS
		return "";
		#endif

		#if UNITY_EDITOR_OSX
		return "";
		#endif
	}
		
	//Authenticate
	public static void authenticate()
	{
		#if UNITY_ANDROID
		PlayGameServices.authenticate();
		#endif
		#if UNITY_IOS
		GameCenterBinding.authenticateLocalPlayer();
		#endif

		#if UNITY_EDITOR
		Debug.Log("Authentication Process wont work in Editor Mode");
		#endif

	}
	//Sign out
	public static void signOut()
	{
		#if UNITY_ANDROID
		PlayGameServices.signOut();
		#endif

	}
		
	//Check if User is signed in
	public static bool isSignedIn()
	{
		#if UNITY_ANDROID
		return PlayGameServices.isSignedIn();
		#endif

		#if UNITY_IOS
		return GameCenterBinding.isPlayerAuthenticated(); 
		#endif

	}

	//Get Player Info
	public static GPGPlayerInfo getLocalPlayerInfo_alias()
	{
		GPGPlayerInfo playerInfoPlayStore = new GPGPlayerInfo();
		#if UNITY_ANDROID
		playerInfoPlayStore = PlayGameServices.getLocalPlayerInfo();
		return playerInfoPlayStore;
		#endif

		#if UNITY_IOS
		playerInfoPlayStore.name = GameCenterBinding.playerAlias();
		return playerInfoPlayStore;

		#endif
	}

	//load specific player
	public static void loadPlayersData( string[] playerIds )
	{
		#if UNITY_ANDROID
		PlayGameServices.loadPlayer(playerIds[0]);
		#endif

		#if UNITY_IOS
		GameCenterBinding.loadPlayerData(playerIds,true,true);
		#endif
	}
	
	
	// Reloads all Play Game Services related metadata
	public static void reloadAchievementAndLeaderboardData()
	{
		#if UNITY_ANDROID
		PlayGameServices.reloadAchievementAndLeaderboardData();
		#endif

		#if UNITY_IOS
		Debug.Log("reloadAchievementAndLeaderboardData Android ONLY");
		#endif
	}
	
	
	// Android only. Loads a profile image from a Uri. Once loaded the profileImageLoadedAtPathEvent will fire.
	public static void loadProfileImage( string uri = null )
	{
		#if UNITY_ANDROID
		PlayGameServices.loadProfileImageForUri(uri);
		#endif

		#if UNITY_IOS
		GameCenterBinding.loadProfilePhotoForLocalPlayer();
		#endif
	}
	
	
	// Shows a native Google+ share dialog with optional prefilled message (iOS only) and optional url to share
	public static void showShareDialog( string prefillText, string urlToShare)
	{
		#if UNITY_ANDROID
		PlayGameServices.showShareDialog(prefillText = null, urlToShare = null);
		#endif

	}

	///Authentications And Settings Completed///
	////////////////////////////////////////////

	////////Leaderboard functions/////
	//////////////////////////////////

	// Shows the requested leaderboard. timeScope is no supported on either platform with the current SDK
// Int \ Enum Mapping
//		    Unknown = -1, 	   //Android
//		    Today = 1,	       //Android	
//		    ThisWeek = 2,      //Android
//		    AllTime = 3        //Android
//			Today = 0,         //iOS
//			Week =1 ,          //iOS
//			AllTime= 2         //iOS

	private static int scopeConversion(Scope scope)
	{

		#if UNITY_ANDROID
		if(scope == Scope.Unknown)
			return -1;
		if(scope == Scope.Today)
			return 1;
		if(scope == Scope.Week)
			return 2;
		if(scope == Scope.AllTime)
			return 3;
		#endif

		#if UNITY_IOS
		if(scope == Scope.Today)
			return 0;
		if(scope == Scope.Week)
			return 1;
		if(scope == Scope.AllTime)
			return 2;
		#endif
		return 5;
	}
 
	public static void showLeaderboard( string leaderboardId  /*Time Scope is For iOS Only*/ )
	{
		#if UNITY_ANDROID
		PlayGameServices.showLeaderboard(leaderboardId);
		#endif
		
		#if UNITY_IOS
		int AllTimeIntVal = 2;
		GameCenterLeaderboardTimeScope AllTime = (GameCenterLeaderboardTimeScope) AllTimeIntVal; 
		GameCenterBinding.showLeaderboardWithTimeScopeAndLeaderboard( AllTime ,leaderboardId);
		#endif
	}
	// Shows a list of all learderboards
	public static void showLeaderboards()
	{
		#if UNITY_ANDROID
		PlayGameServices.showLeaderboards();
		#endif
		
		#if UNITY_IOS
		GameCenterBinding.showGameCenterViewController( GameCenterViewControllerState.Achievements );
		#endif

	}
	
	
	// Submits a score for the given leaderboard with optional scoreTag. Fires the submitScoreFailed/Succeeded event when complete.
	public static void submitScore( string leaderboardId, long score, string scoreTag = ""/* Tag is for Android Only*/ )
	{
		#if UNITY_ANDROID
		PlayGameServices.submitScore( leaderboardId,score,scoreTag);
		#endif
		
		#if UNITY_IOS
		GameCenterBinding.reportScore(score,leaderboardId);
		#endif

	}


	
	// Loads scores for the given leaderboard. Fires the loadScoresFailed/Succeeded event when complete. 
	//Use int Value to paas timeScope
	public static void loadScoresForLeaderboard( string leaderboardId, Scope timeScope, bool isSocial, bool personalWindow )
	{
		#if UNITY_ANDROID
		int scope = scopeConversion(timeScope);
		GPGLeaderboardTimeScope scopeEnum = (GPGLeaderboardTimeScope)scope;
		PlayGameServices.loadScoresForLeaderboard(leaderboardId,scopeEnum,isSocial,personalWindow);
		#endif

		#if UNITY_IOS
		int scope = scopeConversion(timeScope);
		GameCenterLeaderboardTimeScope scopeEnum = (GameCenterLeaderboardTimeScope)scope;
		GameCenterBinding.showLeaderboardWithTimeScopeAndLeaderboard(scopeEnum,leaderboardId);
		#endif

	}
	
	
	// Loads the current players score for the given leaderboard. Fires the loadCurrentPlayerLeaderboardScoreSucceeded/FailedEvent when complete.
	public static void loadCurrentPlayerLeaderboardScore( string leaderboardId, GPGLeaderboardTimeScope timeScope, bool isSocial )
	{
		#if UNITY_ANDROID
		PlayGameServices.loadCurrentPlayerLeaderboardScore(leaderboardId,timeScope,isSocial);
		#endif
		
		#if UNITY_IOS
		Debug.Log("loadCurrentPlayerLeaderboardScore Android ONLY");
		#endif

	}

	//iOS Only Calls

	// Sends a request to get the current scores with the given criteria. End MUST be between 1 and 100 inclusive.
	// Results in the retrieveScoresFailedEvent/scoresLoadedEvent firing and if the local players scores gets loaded retrieveScoresForPlayerIdLoadedEvent will fire.
	public static void retrieveScores( bool friendsOnly, Scope timeScope, int start, int end )
	{

	#if UNITY_IOS
		int scope = scopeConversion(timeScope);
		GameCenterLeaderboardTimeScope scopeEnum = (GameCenterLeaderboardTimeScope)scope;
		GameCenterBinding.retrieveScores(friendsOnly, scopeEnum,start,end);
	#endif

	}
	

	
	// Sends a request to get the current scores with the given criteria. End MUST be between 1 and 100 inclusive.
	// Results in the retrieveScoresFailedEvent/scoresLoadedEvent firing and if the local players scores gets loaded retrieveScoresForPlayerIdLoadedEvent will fire.
	public static void retrieveScores( bool friendsOnly, Scope timeScope, int start, int end, string leaderboardId )
	{

	#if UNITY_IOS
		int scope = scopeConversion(timeScope);
		GameCenterLeaderboardTimeScope scopeEnum = (GameCenterLeaderboardTimeScope)scope;
		GameCenterBinding.retrieveScores(friendsOnly,scopeEnum,start,end,leaderboardId);
	#endif

	}
	

	// Sends a request to get the current scores for the given playerId. retrieveScoresForPlayerIdLoadedEvent/retrieveScoresForPlayerIdFailedEvent will fire with the results.
	public static void retrieveScoresForPlayerId( string playerId )
	{

	#if UNITY_IOS
		GameCenterBinding.retrieveScoresForPlayerId(playerId);
	#endif

	}
	

	// Sends a request to get the current scores for the given playerId and leaderboardId. retrieveScoresForPlayerIdLoadedEvent/retrieveScoresForPlayerIdFailedEvent will fire with the results.
	public static void retrieveScoresForPlayerId( string playerId, string leaderboardId )
	{

	#if UNITY_IOS
		GameCenterBinding.retrieveScoresForPlayerId(playerId,leaderboardId);
	#endif

	}
	

	// Sends a request to get the current scores for the given playerIds and leaderboardId. retrieveScoresForPlayerIdLoadedEvent/retrieveScoresForPlayerIdFailedEvent will fire with the results.
	public static void retrieveScoresForPlayerIds( string[] playerIdArray, string leaderboardId )
	{

	#if UNITY_IOS
		GameCenterBinding.retrieveScoresForPlayerIds(playerIdArray,leaderboardId);
	#endif

	}
	
	
	////////Leaderboard functions Completed/////
	////////////////////////////////////////////



	////////Achievements functions Started /////
	////////////////////////////////////////////
//fix
	// Reports an achievement with the given identifier and percent complete
	public static void reportAchievement( string identifier, float percent, int steps /*Steps for android call*/ )
	{
		#if UNITY_ANDROID
		if (steps != 0)
			PlayGameServices.incrementAchievement(identifier,steps);
		else 
			PlayGameServices.unlockAchievement(identifier);
		#endif
		
		#if UNITY_IOS
			GameCenterBinding.reportAchievement(identifier,percent);
		#endif
	}
	
	

	
	// Sends a request to get a list of all the current achievements for the authenticated player.
	public static void getAchievements()
	{

		#if UNITY_IOS
		GameCenterBinding.getAchievements();
		#endif
	}
	//Reveal Achievemt if previously hidden
	public static void revealAchievement( string achievementId )
	{
		#if UNITY_ANDROID
		PlayGameServices.revealAchievement(achievementId);
		#endif
	}
		

	// Resets all the achievements for the authenticated player.
	public static void resetAchievements()
	{
		
		#if UNITY_IOS
		GameCenterBinding.resetAchievements();
		#endif
	}
	
	
		
	// Shows the standard, GameCenter achievement list
	public static void showAchievements()
	{
		#if UNITY_ANDROID
		PlayGameServices.showAchievements();
		#endif
		
		#if UNITY_IOS
		GameCenterBinding.showAchievements();
		#endif
	}
	

	// Sends a request to get the achievements for the current game.
	public static void retrieveAchievementMetadata()
	{
		#if UNITY_ANDROID
		PlayGameServices.getAllAchievementMetadata();
		#endif
		
		#if UNITY_IOS
		GameCenterBinding.retrieveAchievementMetadata();
		#endif
	}
	


	////////Achievements functions Completed ///
	////////////////////////////////////////////

	
	
	
	////////////////////////////////////////////
	/// Game Center and GPGS Calls Completed////
	////////////////////////////////////////////



	/////////////////////////////////////////////////
	/// Batch Push Notification functions Starts ////
	/////////////////////////////////////////////////








	
	
}
